# language: ca

#noinspection SpellCheckingInspection
Característica: Introduir reserva

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I inicio el torn al tpv com a "Joan" amb contrasenya "1234"
    I un producte amb nom "Optimus Prime", preu 30€, iva 10% i codi de barres 1234567
    I un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres 2345671 que encara no esta a la venda

  Escenari: Fer reserva d'un producte a la venda
    Quan introdueix una reserva del client amb dni 77747742 per el producte amb nom "Optimus Prime" amb codi de barres 1234567 en quantitat 1
    Aleshores obtinc un error que diu: "Producte ja disponible a la botiga"

  Escenari: Fer reserva d'un producte que no existeix
    Quan introdueix una reserva del client amb dni 77747742 per el producte amb nom "Pilota" amb codi de barres 1234567 en quantitat 1
    Aleshores obtinc un error que diu: "El producte a reservar no existeix"

  Escenari: Fer reserva d'un producte que encara no està a la venda
    Quan introdueix una reserva del client amb dni 77747742 per el producte amb nom "Gollum" amb codi de barres 2345671 en quantitat 1
    Aleshores hi ha una reserva al sistema pel producte amb nom "Gollum" i el dni 77747742

