# language: ca

#noinspection SpellCheckingInspection
Característica: Descanviar punts de soci per un descompte

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que en "Joan" ha iniciat el torn al tpv
    I hi ha una gestio de socis
    I introdueixo les dades nom "Marc", cognom "Martinez", edat 23 i telèfon 99999

  Escenari: Consultar punts del soci
    Donat en el sistema hi ha un soci amb nom "Marc", cognom "Martinez", edat 23, telèfon 99999 i 50 punts
    Quan vull consultar quants diners corresponen a aquests 50 punts
    Aleshores la pantalla de tpv mostra
    """
    50 punts corresponen 5€
    """


  Escenari: Descanviar punts per descompte
    Donat en el sistema hi ha un soci amb nom "Marc", cognom "Martinez", edat 23, telèfon 99999 i 50 punts
    Quan vull descanviar punts de soci amb nom "Marc", cognom "Martinez", edat 23, telèfon 99999 i 50 punts per un descompte
    Aleshores el soci amb nom "Marc", cognom "Martinez", edat 23, telèfon 99999 i 50 punts té el descompte "Val descompte 5€"



