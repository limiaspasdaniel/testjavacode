# language: ca

#noinspection SpellCheckingInspection
Característica: Crear nou soci

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que en "Joan" ha iniciat el torn al tpv
    I hi ha una gestio de socis
    I que hi ha al sistema un soci amb nom "Miki", cognom "Nadal", edat 30 i telèfon 66666

  Escenari: Crear nou soci
    Quan introdueixo les dades nom "Marc", cognom "Martinez", edat 23 i telèfon 99999
    Aleshores hi ha un soci amb nom "Marc", cognom "Martinez", edat 23, telèfon 99999 i 0 punts

  Escenari: No es pot crear un soci si aquest ja existeix
    Quan introdueixo les dades nom "Miki", cognom "Nadal", edat 30 i telèfon 66666
    Aleshores obtinc un error que diu: "Aquest soci ja existeix"