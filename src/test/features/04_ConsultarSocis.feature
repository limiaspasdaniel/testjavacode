# language: ca

#noinspection SpellCheckingInspection
Característica: Consultar tots socis existents en el sistema

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que en "Joan" ha iniciat el torn al tpv
    I hi ha una gestio de socis
    I un soci amb nom "Marc", cognom "Martinez", edat 23 i telefon 99999
    I "Marc" "Martinez" 99999 fa una compra a la data "20-11-2015" i hora "11:00:00"
    I "Marc" "Martinez" 99999 fa una compra a la data "25-12-2015" i hora "21:00:00"
    I un soci amb nom "Josep", cognom "Codina", edat 34 i telefon 88888
    I "Josep" "Codina" 88888 fa una compra a la data "20-12-2015" i hora "11:10:00"

  Escenari: Mostrar tots els socis
    Quan el gestor vol veure tots els socis
    Aleshores hi haura/n 2 socis
    I hi ha un soci amb nom "Marc", cognom "Martinez", edat 23, telefon 99999 i 0 punts
    I hi ha un soci amb nom "Josep", cognom "Codina", edat 34, telefon 88888 i 0 punts
    I sobre la pantalla mostra tots els socis
    """
    NOM COGNOM EDAT TELEFON PUNTS
    ---
    Marc Martinez 23 99999 0
    Josep Codina 34 88888 0
    """
  Escenari: Seleccionar un soci per veure mes detalls
    Quan el gestor vol veure tots els socis
    I el gestor vol veure detalls del soci "Marc" "Martinez" 99999
    Aleshores hi ha un soci amb nom "Marc", cognom "Martinez", edat 23, telefon 99999 i 0 punts
    I sobre la pantalla mostra el soci seleccionat detallat
    """
    NOM COGNOM EDAT TELEFON PUNTS
    ---
    Marc Martinez 23 99999 0
    ---
    Ha fet les compres:
    DATA HORA BOTIGA TPV VENEDOR
    20-11-2015 11:00:00 Girona 1 1 Joan
    25-12-2015 21:00:00 Girona 1 1 Joan
    """