# language: ca

#noinspection SpellCheckingInspection
Característica: Introduir val de descompte a una venda

  Rerefons:
    Donat un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Gothmog", preu 20€, iva 21% i codi de barres 1234568
    I un producte amb nom "Arturito", preu 10€, iva 21% i codi de barres 1234569
    I que estem al tpv número 1 de la botiga "Girona 1"
    I que en "Joan" ha iniciat el torn al tpv
    I que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que existeix un descompte de 2x1 amb nom "2x1 en figuretes de Star Wars" assignat al producte amb nom "Arturito"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I que he afegit el producte de codi de barres 1234567 a la venta
    I que he afegit el producte de codi de barres 1234568 a la venta
    I que he afegit el producte de codi de barres 1234569 a la venta
    I que he afegit el producte de codi de barres 1234569 a la venta
    I línia de venta 1 és de 2 unitats de "Optimus Prime" a 23€ cada una per un total de 46€
    I línia de venta 2 és de 1 unitats de "Gothmog" a 20€ cada una per un total de 20€
    I línia de venta 3 és de 2 unitats de "Arturito" a 10€ cada una per un total de 20€
    I el total de la venta actual és de 76€

    I un val de descompte d'import total de 5€ amb nom "val 5€", codi 7654321, compra mínima 30€, "no usat" i que caduca el "22-11-2015"
    I un val de descompte d'import total de 25€ amb nom "val 25€", codi 7654322, compra mínima 90€, "no usat" i que caduca el "22-11-2015"

    I un val de descompte de percentatge total del 10% amb nom "val 10%", codi 7654323, compra mínima 10€, "no usat" i que caduca el "25-11-2015"
    I un val de descompte de percentatge total del 20% amb nom "val 20%", codi 7654324, compra mínima 20€, "usat" i que caduca el "22-11-2015"

    I un val de descompte de 2x1 en "Optimus Prime" amb nom "val 2x1 Optimus Prime", codi 7654325, "no usat" i que caduca el "26-11-2015"
    I un val de descompte de 3x2 en "Optimus Prime" amb nom "val 3x2 Optimus Prime", codi 7654326, "no usat" i que caduca el "19-11-2015"
    I un val de descompte de 3x2 en "Gothmog" amb nom "val 3x2 Gothmog", codi 7654327, "no usat" i que caduca el "26-11-2015"
    I un val de descompte de 2x1 en "Arturito" amb nom "val 2x1 Arturito", codi 7654328, "no usat" i que caduca el "30-11-2015"

  Escenari: Afegir un val de descompte d'import total a una venda
    Quan afegeixo el val de descompte amb codi 7654321 a la venda
    Aleshores el val de descompte de codi 7654321 està aplicat a la venda amb import descomptat 5€
    I el val de descompte amb codi 7654321 està "usat"
    I el total de la venta actual és de 71€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 2u = 46€
    Gothmog - 20€/u x 1u = 20€
    Arturito - 10€/u x 2u = 20€
        Descompte: 2x1 en figuretes de Star Wars = -10€
    ---
    val 5€ = -5€
    ---
    Total: 71€
    """

  Escenari: Afegir un val de descompte de percentatge total a una venda
    Quan afegeixo el val de descompte amb codi 7654323 a la venda
    Aleshores el val de descompte de codi 7654323 està aplicat a la venda amb import descomptat 8€
    I el val de descompte amb codi 7654323 està "usat"
    I el total de la venta actual és de 68€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 2u = 46€
    Gothmog - 20€/u x 1u = 20€
    Arturito - 10€/u x 2u = 20€
        Descompte: 2x1 en figuretes de Star Wars = -10€
    ---
    val 10% = -8€
    ---
    Total: 68€
    """

  Escenari: Afegir producte a la venda després d'haver afegit un val de descompte de percentatge total
    Donat que he afegit el val de descompte amb codi 7654323 a la venda
    I el val de descompte de codi 7654323 està aplicat a la venda amb import descomptat 8€
    I el total de la venta actual és de 68€
    I el val de descompte amb codi 7654323 està "usat"
    Quan afegeixo el producte de codi de barres 1234568 a la venta
    I el val de descompte de codi 7654323 està aplicat a la venda amb import descomptat 10€
    Aleshores el total de la venta actual és de 86€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 2u = 46€
    Gothmog - 20€/u x 2u = 40€
    Arturito - 10€/u x 2u = 20€
        Descompte: 2x1 en figuretes de Star Wars = -10€
    ---
    val 10% = -10€
    ---
    Total: 86€
    """

  Escenari: Afegir un val de descompte de XxY a una venda
    Quan afegeixo el val de descompte amb codi 7654325 a la venda
    Aleshores el val de descompte de codi 7654325 està aplicat a la venda amb import descomptat 23€
    I el val de descompte amb codi 7654325 està "usat"
    I el total de la venta actual és de 53€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 2u = 46€
    Gothmog - 20€/u x 1u = 20€
    Arturito - 10€/u x 2u = 20€
        Descompte: 2x1 en figuretes de Star Wars = -10€
    ---
    val 2x1 Optimus Prime = -23€
    ---
    Total: 53€
    """

  Escenari: Afegir un val de descompte de XxY sense haver afegit a la venda les unitats necessàries del producte
    Quan afegeixo el val de descompte amb codi 7654327 a la venda
    Aleshores obtinc un error que diu: "No hi ha prou unitats del producte del val de descompte"

  Escenari: Afegir un val de descompte en una venda inexistent al sistema
    Quan afegeixo el val de descompte amb codi 6668666 a la venda
    Aleshores obtinc un error que diu: "Aquest codi no correspon a cap val de descompte registrat al sistema"

  Escenari: Afegir un val de descompte caducat a una venda
    Quan afegeixo el val de descompte amb codi 7654326 a la venda
    Aleshores obtinc un error que diu: "El codi introduït correspon a un val de descompte caducat"

  Escenari: Afegir un val de descompte a una venda amb preu actual per sota de la compra mínima del val
    Quan afegeixo el val de descompte amb codi 7654322 a la venda
    Aleshores obtinc un error que diu: "El total de la venda actual és inferior al mínim requerit pel val de descompte"

  Escenari: Afegir un val de descompte usat a una venda
    Quan afegeixo el val de descompte amb codi 7654324 a la venda
    Aleshores obtinc un error que diu: "El codi introduït correspon a un val de descompte ja utilitzat"

  Escenari: No es pot afegir un val de descompte de XxY sobre un producte que ja té assignat un descompte d'aquest tipus
    Quan afegeixo el val de descompte amb codi 7654328 a la venda
    Aleshores obtinc un error que diu: "El producte del val ja té un descompte de XxY associat"