# language: ca

#noinspection SpellCheckingInspection
Característica: Afegir devolucions

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I que l'usuari "Joan" amb contrasenya "1234" ha iniciat el torn al tpv
    I la data es "30-Nove-15"
    I que el recompte inicial del dia és 0€ a dia "30/11/2015"
    I un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Primo Optimo", preu 2€, iva 21% i codi de barres 7654321
    I un soci amb nom "Marc", cognom "Martinez", edat 23 i telefon 99999
    I un soci amb nom "Pedro", cognom "Banana", edat 50 i telefon 12345
    I durant el dia es realitzen vendes, generacio automatica: 230€

  Escenari: Afegir una devolucio per un soci i vol val, una per un soci i no vol val, una per un no soci i vol val, i una per un no soci i no vol val, amb exit
    Quan "Marc" "Martinez" 99999 vol fer una devolucio de productes
    I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol un val a les "12:30:59" amb el motiu "Devolucio de Optimus Prime i Primo Optimo"

    I "Pedro" "Banana" 12345 vol fer una devolucio de productes
    I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
    I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
    I no vol val a les "12:40:00" amb el motiu "Devolucio dos Optimus Prime"

    I un client no soci vol fer una devolucio de productes
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol un val a les "12:50:30" amb el motiu "Devolucio dos Primo Optimo"

    I un client no soci vol fer una devolucio de productes
    I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
    I no vol val a les "13:55:59" amb el motiu "Devolucio de Optimus Prime"

    I vol veure les devolucions
    Aleshores el quadrament del dia es 156€
    I a la pantalla mostra un llistat de tots els devolucions
    """
    NOM I COGNOM DEL SOCI | QUANTITAT(€) DE DINERS | MOTIU DEVOLUCIO | DATA I HORA DEVOLUCIO | NUMERO TPV | BOTIGA | VENEDOR | HA DEMANAT VAL DEVOLUCIO
    Marc Martinez | 29.0 | Devolucio de Optimus Prime i Primo Optimo | 30-Nove-15 12:30:59 | 1 | Girona | Joan | SI
    PRODUCTES RETORNADES:
    Optimus Prime
    Primo Optimo

    Pedro Banana | 54.0 | Devolucio dos Optimus Prime | 30-Nove-15 12:40:00 | 1 | Girona | Joan | NO
    PRODUCTES RETORNADES:
    Optimus Prime
    Optimus Prime

    NO ES SOCI | 4.0 | Devolucio dos Primo Optimo | 30-Nove-15 12:50:30 | 1 | Girona | Joan | SI
    PRODUCTES RETORNADES:
    Primo Optimo
    Primo Optimo

    NO ES SOCI | 27.0 | Devolucio de Optimus Prime | 30-Nove-15 13:55:59 | 1 | Girona | Joan | NO
    PRODUCTES RETORNADES:
    Optimus Prime


    """

    Escenari: Afegir dues devolucions en el mateix temps sense exit
      Quan "Marc" "Martinez" 99999 vol fer una devolucio de productes
      I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
      I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
      I no vol val a les "12:30:59" amb el motiu "Devolucio de Optimus Prime i Primo Optimo"

      I "Pedro" "Banana" 12345 vol fer una devolucio de productes
      I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
      I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
      I no vol val a les "12:30:59" amb el motiu "Devolucio dos Optimus Prime"

      Aleshores obtinc un error que diu: "Ja existeix devolucio amb la data, hora i tpv donada"

  Escenari: Afegir una devolucio sense productes
    Quan "Marc" "Martinez" 99999 vol fer una devolucio de productes
    I no vol val a les "12:30:59" amb el motiu "Devolucio de Optimus Prime i Primo Optimo"

    Aleshores obtinc un error que diu: "No hi ha productes retornades"