# language: ca

#noinspection SpellCheckingInspection
Característica: Fer descomptes en certs productes
  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que en "Joan" ha iniciat el torn al tpv
    I que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que el producte nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567 ja existeix
    I que el producte nom "Adidas Soccer Ball", preu 30€, iva 14% i codi de barres 111111 ja existeix
    I que el producte nom "Nike Game Ball", preu 30€, iva 14% i codi de barres 222222 ja existeix
    I que el producte nom "Optimus Ultim", preu 30€, iva 21% i codi de barres 7654321 ja existeix

  Escenari: Aplicar un descompte amb percentatge
    Donat es dona d'alta un descompte amb percentatge amb nom "Percentatge 20%" i percentatge 20
    I s'assigna al producte amb nom "Optimus Prime" el descompte amb nom "Percentatge 20%"
    I s'assigna al producte amb nom "Adidas Soccer Ball" el descompte amb nom "Percentatge 20%"
    Quan he afegit el producte de codi de barres 1234567 a la venta amb 1 unitat
    I he afegit el producte de codi de barres 111111 a la venta amb 1 unitat
    I he afegit el producte de codi de barres 7654321 a la venta amb 1 unitat
    Aleshores la pantalla del tpv mostra
    """
    Llista de tots els productes:
    Adidas Soccer Ball
    Nike Game Ball
    Optimus Prime
    Optimus Ultim
    ----
    Llista de productes comprats:
    1: Optimus Prime ------ 23 * 1 unit(s) = 23€
    2: Adidas Soccer Ball ------ 30 * 1 unit(s) = 30€
    3: Optimus Ultim ------ 30 * 1 unit(s) = 30€
    ----
    Descomptes aplicats:
    Percentatge 20%:
    Optimus Prime x 1u = -5€
    Adidas Soccer Ball x 1u = -6€
    *Total aplicant descomptes*: 72€
    """

  Escenari: Aplicar un descompte de X per Y
    Donat es dona d'alta un descompte de X per Y amb nom "3x2", X: 3 i Y: 2
    I s'assigna al producte amb nom "Nike Game Ball" el descompte amb nom "3x2"
    I s'assigna al producte amb nom "Optimus Prime" el descompte amb nom "3x2"
    Quan he afegit el producte de codi de barres 1234567 a la venta amb 3 unitat
    I he afegit el producte de codi de barres 222222 a la venta amb 3 unitat
    Aleshores la pantalla del tpv mostra
    """
    Llista de tots els productes:
    Adidas Soccer Ball
    Nike Game Ball
    Optimus Prime
    Optimus Ultim
    ----
    Llista de productes comprats:
    1: Optimus Prime ------ 23 * 3 unit(s) = 69€
    2: Nike Game Ball ------ 30 * 3 unit(s) = 90€
    ----
    Descomptes aplicats:
    3x2:
    Optimus Prime x 3u = -23€
    Nike Game Ball x 3u = -30€
    *Total aplicant descomptes*: 106€
    """

  Escenari: Fer un val de descompte
    Donat es dona d'alta un descompte de val amb nom "Val 20€" i val: 20
    I s'assigna al producte amb nom "Nike Game Ball" el descompte amb nom "Val 20€"
    Quan he afegit el producte de codi de barres 222222 a la venta amb 2 unitat
    I he afegit el producte de codi de barres 7654321 a la venta amb 1 unitat
    Aleshores la pantalla del tpv mostra
    """
    Llista de tots els productes:
    Adidas Soccer Ball
    Nike Game Ball
    Optimus Prime
    Optimus Ultim
    ----
    Llista de productes comprats:
    1: Nike Game Ball ------ 30 * 2 unit(s) = 60€
    2: Optimus Ultim ------ 30 * 1 unit(s) = 30€
    ----
    Descomptes aplicats:
    Val 20€:
    Nike Game Ball x 2u = -20€
    *Total aplicant descomptes*: 70€
    """

  Escenari: No es pot aplicar un descompte a un producte que ja té un descompte
    Donat es dona d'alta un descompte amb percentatge amb nom "Percentatge 20%" i percentatge 20
    I s'assigna al producte amb nom "Optimus Prime" el descompte amb nom "Percentatge 20%"
    I s'assigna al producte amb nom "Optimus Prime" el descompte amb nom "3x2"
    Aleshores obtinc un error que diu: "Aquest producte ja té un descompte al sistema"


