# language: ca

#noinspection SpellCheckingInspection
Característica: Afegir nou producte al sistema

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"

  Escenari: Afegir nou producte
    Quan tenim un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres 2345671
    Aleshores el sistema indica que el producte amb codi barres "2345671" s'ha afegit correctament

  Escenari: Afegir producte amb un codi de barres ja existent
    Quan tenim un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres 2345671
    I tenim un producte amb nom "Optimus Prime", preu 30€, iva 15% i codi de barres 2345671
    Aleshores obtinc un error que diu: "Ja existeix un producte amb codi de barres 2345671."

  Escenari: Afegir producte amb un codi de barres incorrecte
    Quan tenim un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres -2345671
    Aleshores obtinc un error que diu: "ERROR: Codi de barres menor que 0."

  Escenari: Afegir producte amb un preu incorrecte
    Quan tenim un producte amb nom "Gollum", preu -20€, iva 10% i codi de barres 2345671
    Aleshores obtinc un error que diu: "ERROR: Preu menor que 0."

  Escenari: Afegir producte amb un iva incorrecte
    Quan tenim un producte amb nom "Gollum", preu 20€, iva -10% i codi de barres 2345671
    Aleshores obtinc un error que diu: "ERROR: IVA menor que 0."