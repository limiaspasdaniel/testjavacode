# language: ca

#noinspection SpellCheckingInspection
Característica: Fer LogOut del torn

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I que l'usuari "Joan" amb contrasenya "1234" ha iniciat el torn al tpv

  Escenari: Fer LogOut del torn
    Quan faig LogOut del tpv
    Aleshores ningú està utilitzant el tpv

  Escenari: Fer LogOut del torn i provar d'iniciar una nova venta
    Quan faig LogOut del tpv
    I inicio una nova venta a la data "20-11-2015" i hora "11:00:00"
    Aleshores obtinc un error que diu: "No es pot iniciar una venta sense haver iniciat el torn!"

