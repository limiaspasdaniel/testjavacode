# language: ca

#noinspection SpellCheckingInspection
Característica: Imprimir el tiquet

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona"
    I que en "Joan" ha iniciat el torn al tpv
    I que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I un producte amb nom "Spalding Game Ball", preu 40€, iva 21% i codi de barres 000000
    I un producte amb nom "Adidas Soccer Ball", preu 30€, iva 14% i codi de barres 111111
    I un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Optimus Ultim", preu 30€, iva 21% i codi de barres 7654321
    I un producte amb nom "Nike Game Ball", preu 30€, iva 14% i codi de barres 222222
    I es dona d'alta un descompte amb percentatge amb nom "Percentatge 20%" i percentatge 20
    I es dona d'alta un descompte de X per Y amb nom "3x2", X: 3 i Y: 2
    I es dona d'alta un descompte de val amb nom "Val 20€" i val: 20

  Escenari: Imprimir tiquet sense descompte
    Donat que he afegit el producte de codi de barres 1234567 a la venta
    I que he afegit el producte de codi de barres 111111 a la venta
    I que he afegit el producte de codi de barres 000000 a la venta
    I que he afegit el producte de codi de barres 7654321 a la venta
    Quan indico que el client ha entregat 150€ per a pagar en metàlic
    Aleshores imprimeixo el ticket de la venda
    """
    1: Optimus Prime ------ 23 * 1 unit(s) = 23€
    2: Adidas Soccer Ball ------ 30 * 1 unit(s) = 30€
    3: Spalding Game Ball ------ 40 * 1 unit(s) = 40€
    4: Optimus Ultim ------ 30 * 1 unit(s) = 30€
    ----
    I.V.A:
    76€ + 21%: 93€
    26€ + 14%: 30€
    ----
    Total sense descompte: 123€
    Total amb descompte: 123€
    Entregat: 150€
    Canvi: 27€
    Tipus de pagament: Metalic
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """

  Escenari: Imprimir tiquet amb descompte percentatge
    Donat el descompte "Percentatge 20%" existeix a la llistat de descomptes
    I s'assigna al producte amb nom "Optimus Prime" el descompte amb nom "Percentatge 20%"
    I s'assigna al producte amb nom "Adidas Soccer Ball" el descompte amb nom "Percentatge 20%"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I que he afegit el producte de codi de barres 111111 a la venta
    I que he afegit el producte de codi de barres 7654321 a la venta
    Quan indico que el client ha entregat 100€ per a pagar en metàlic
    Aleshores imprimeixo el ticket de la venda
    """
    1: Optimus Prime ------ 23 * 1 unit(s) = 23€
    2: Adidas Soccer Ball ------ 30 * 1 unit(s) = 30€
    3: Optimus Ultim ------ 30 * 1 unit(s) = 30€
    ----
    I.V.A:
    43€ + 21%: 53€
    26€ + 14%: 30€
    ----
    Total sense descompte: 83€
    Percentatge 20%:
    Optimus Prime x 1u = -5€
    Adidas Soccer Ball x 1u = -6€
    Total amb descompte: 72€
    Entregat: 100€
    Canvi: 28€
    Tipus de pagament: Metalic
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """

  Escenari: Imprimir tiquet amb descompte X per Y
    Donat el descompte "3x2" existeix a la llistat de descomptes
    I s'assigna al producte amb nom "Optimus Prime" el descompte amb nom "3x2"
    I s'assigna al producte amb nom "Nike Game Ball" el descompte amb nom "3x2"
    I he afegit el producte de codi de barres 1234567 a la venta amb 3 unitat
    I he afegit el producte de codi de barres 222222 a la venta amb 3 unitat
    Quan indico que el client ha entregat 200€ per a pagar en metàlic
    Aleshores imprimeixo el ticket de la venda
    """
    1: Optimus Prime ------ 23 * 3 unit(s) = 69€
    2: Nike Game Ball ------ 30 * 3 unit(s) = 90€
    ----
    I.V.A:
    57€ + 21%: 69€
    78€ + 14%: 90€
    ----
    Total sense descompte: 159€
    3x2:
    Optimus Prime x 3u = -23€
    Nike Game Ball x 3u = -30€
    Total amb descompte: 106€
    Entregat: 200€
    Canvi: 94€
    Tipus de pagament: Metalic
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """

  Escenari: Imprimir tiquet amb val descompte
    Donat el descompte "Val 20€" existeix a la llistat de descomptes
    I s'assigna al producte amb nom "Nike Game Ball" el descompte amb nom "Val 20€"
    I he afegit el producte de codi de barres 222222 a la venta amb 2 unitat
    I he afegit el producte de codi de barres 7654321 a la venta amb 1 unitat
    Quan indico que el client ha entregat 100€ per a pagar en metàlic
    Aleshores imprimeixo el ticket de la venda
    """
    1: Nike Game Ball ------ 30 * 2 unit(s) = 60€
    2: Optimus Ultim ------ 30 * 1 unit(s) = 30€
    ----
    I.V.A:
    52€ + 14%: 60€
    24€ + 21%: 30€
    ----
    Total sense descompte: 90€
    Val 20€:
    Nike Game Ball x 2u = -20€
    Total amb descompte: 70€
    Entregat: 100€
    Canvi: 30€
    Tipus de pagament: Metalic
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """

  Escenari: Imprimir tiquet amb val descompte i descompte percentatge
    Donat el descompte "Val 20€" existeix a la llistat de descomptes
    I el descompte "Percentatge 20%" existeix a la llistat de descomptes
    I s'assigna al producte amb nom "Optimus Prime" el descompte amb nom "Percentatge 20%"
    I s'assigna al producte amb nom "Nike Game Ball" el descompte amb nom "Val 20€"
    I que he afegit el producte de codi de barres 222222 a la venta
    I que he afegit el producte de codi de barres 1234567 a la venta
    Quan indico que el client ha pagat 28€ amb targeta
    Aleshores imprimeixo el ticket de la venda
    """
    1: Nike Game Ball ------ 30 * 1 unit(s) = 30€
    2: Optimus Prime ------ 23 * 1 unit(s) = 23€
    ----
    I.V.A:
    26€ + 14%: 30€
    19€ + 21%: 23€
    ----
    Total sense descompte: 53€
    Val 20€:
    Nike Game Ball x 1u = -20€
    Percentatge 20%:
    Optimus Prime x 1u = -5€
    Total amb descompte: 28€
    Entregat: 28€
    Canvi: 0€
    Tipus de pagament: Targeta
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """