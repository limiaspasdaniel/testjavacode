# language: ca

#noinspection SpellCheckingInspection
Característica: Introduir quadrament

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I inicio el torn al tpv com a "Joan" amb contrasenya "1234"
    I que el recompte inicial del dia és 100€ a dia "21/12/2015"
    I inicio una nova venta a la data "20-11-2015" i hora "11:00:00"
    I un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres 2345671
    I un producte amb nom "Optimus Prime", preu 30€, iva 10% i codi de barres 1234567
    I que he afegit el producte de codi de barres 1234567 a la venta
    I que he afegit el producte de codi de barres 2345671 a la venta
    I indico que el client ha entregat 55€ per a pagar en metàlic

  Escenari: Introuduir quadrament correcte
    Quan introdueix l'efectiu de la caixa al final del dia que és 155€
    Aleshores la pantalla mostra un missatge que diu: "Quadrament realitzat amb èxit"
    I El quadrament queda enregistrat al sistema

  Escenari: Introduir un quadrament incorrecte per menys de 1€
    Quan introdueix l'efectiu de la caixa al final del dia que és "155.45"€
    Aleshores la pantalla mostra un missatge que diu: "Quadrament incorrecte per menys de 1€"

  Escenari: Introduir un quadrament incorrecte per més de 1€
    Quan introdueix l'efectiu de la caixa al final del dia que és "156.86"€
    Aleshores la pantalla mostra un missatge que diu: "Quadrament incorrecte per més de 1€"

  Escenari: Reintentar quadrament amb encert
    Quan introdueix l'efectiu de la caixa al final del dia que és "156.86"€
    I la pantalla mostra un missatge que diu: "Quadrament incorrecte per més de 1€"
    I introdueix l'efectiu de la caixa al final del dia que és 155€
    Aleshores la pantalla mostra un missatge que diu: "Quadrament realitzat amb èxit"
    I El quadrament queda enregistrat al sistema

  Escenari: Reintentar quadrament i tornar a fallar
    Quan introdueix l'efectiu de la caixa al final del dia que és "156.86"€
    I la pantalla mostra un missatge que diu: "Quadrament incorrecte per més de 1€"
    I introdueix l'efectiu de la caixa al final del dia que és "155.25"€
    Aleshores la pantalla mostra un missatge que diu: "Quadrament incorrecte per menys de 1€"

  Escenari: Enregistar quadrament incorrecte
    Quan introdueix l'efectiu de la caixa al final del dia que és "155.25"€
    Quan el venedor decideix tancar el quadrament tot i ser incorrecte
    Aleshores la pantalla mostra un missatge que diu: "Quadrament incorrecte realitzat"
    I El quadrament queda enregistrat al sistema
