# language: ca

#noinspection SpellCheckingInspection
Característica: Llistar reservs

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I inicio el torn al tpv com a "Joan" amb contrasenya "1234"
    I un producte amb nom "Optimus Prime", preu 30€, iva 10% i codi de barres 1234567 que encara no esta a la venda
    I un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres 2345671 que encara no esta a la venda
    I una reserva al sistema pel producte "Optimus Prime", dni 77747742 i codi de barres 1234567 en quantitat 1
    I una reserva al sistema pel producte "Optimus Prime", dni 99999944 i codi de barres 1234567 en quantitat 2
    I una reserva al sistema pel producte "Gollum", dni 77747742 i codi de barres 2345671 en quantitat 1

  Escenari: Llistar totes les reserves
    Quan visualitzo el llistat de totes les reserves del sistema
    Aleshores obtinc una llista que diu: "1, 77747742, 1;1, 99999944, 2;2, 77747742, 1;"

  Escenari: Llistar reserves d'un producte concret
    Quan visualitzo el llistat de les reserves del producte amb id 1 del sistema
    Aleshores per al producte amb id 1 obtinc una llista que diu: "77747742, 1;99999944, 2;"

