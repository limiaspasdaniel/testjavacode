# language: ca

#noinspection SpellCheckingInspection
Característica: Iniciar una venta

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I que l'usuari "Joan" amb contrasenya "1234" ha iniciat el torn al tpv

  Escenari: Iniciar una venta
    Quan inicio una nova venta a la data "20-11-2015" i hora "11:00:00"
    Aleshores la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la venta té 0 línies
    I el total de la venta actual és de 0€
    I la pantalla del client del tpv mostra
    """
    Li donem la benvinguda a Joguets i Joguines!
    L'atén Joan
    20-11-2015 11:00:00
    """

  Escenari: No es pot iniciar una venta si ja hi ha una venta iniciada
    Donat que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    Quan inicio una nova venta a la data "20-11-2015" i hora "11:00:00"
    Aleshores obtinc un error que diu: "Aquest tpv ja té una venta iniciada"