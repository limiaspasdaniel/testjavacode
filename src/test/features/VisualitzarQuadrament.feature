# language: ca

#noinspection SpellCheckingInspection
Característica: Visualitzar quadrament

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I inicio el torn al tpv com a "Joan" amb contrasenya "1234"
    I que el recompte inicial del dia és 100€ a dia "21/12/2015"
    I inicio una nova venta a la data "20-11-2015" i hora "11:00:00"
    I un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres 2345671
    I un producte amb nom "Optimus Prime", preu 30€, iva 10% i codi de barres 1234567
    I que he afegit el producte de codi de barres 1234567 a la venta
    I que he afegit el producte de codi de barres 2345671 a la venta
    I indico que el client ha entregat 65€ per a pagar en metàlic
    I introdueix l'efectiu de la caixa al final del dia que és 155€

    Donat que el recompte inicial del dia és 150€ a dia "22/12/2015"
    I inicio una nova venta a la data "22-12-2015" i hora "12:00:00"
    I un producte amb nom "Gollum 2", preu 40€, iva 10% i codi de barres 2345672
    I un producte amb nom "Optimus Prime 2", preu 80€, iva 10% i codi de barres 1234568
    I que he afegit el producte de codi de barres 1234568 a la venta
    I que he afegit el producte de codi de barres 2345672 a la venta
    I indico que el client ha entregat 128€ per a pagar en metàlic
    I introdueix l'efectiu de la caixa al final del dia que és 205€



  Escenari: Visualitzar quadraments
    Quan visualitzo els quadraments
    Aleshores aquests són "21-December-15 nom: Joan, tpv: 1, diferencia: 0.0€;22-December-15 nom: Joan, tpv: 1, diferencia: -77.0€;"

