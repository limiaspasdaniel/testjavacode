# language: ca

#noinspection SpellCheckingInspection
Característica: Gestionar venedors

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1"
    I que existeix el gestor "Dani" amb contrasenya "1234"
    I que existeix el venedor "Jordi" amb contrasenya "1111"
    I que existeix el venedor "Oscar" amb contrasenya "2222"
    I que l'usuari "Dani" amb contrasenya "1234" ha iniciat el torn al tpv

  Escenari: Crear nou venedor
    Quan introdueixo les dades nom "Pep" i password "3333"
    Aleshores hi ha un venedor amb nom "Pep" i password "3333"

  Escenari: No es pot crear un venedor si aquest ja existeix
    Quan introdueixo les dades nom "Jordi" i password "1111"
    Aleshores obtinc un error que diu: "Aquest usuari ja existeix"

  Escenari: Llistar venedors
    Quan visualitzo el llistat de tots els venedors del sistema
    Aleshores obtinc un missatge que diu: "Jordi;Oscar;"