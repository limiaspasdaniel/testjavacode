# language: ca

#noinspection SpellCheckingInspection
Característica: Crear vals de devolucio

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I que l'usuari "Joan" amb contrasenya "1234" ha iniciat el torn al tpv
    I la data es "30-Nove-15"
    I un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Primo Optimo", preu 2€, iva 21% i codi de barres 7654321
    I un soci amb nom "Marc", cognom "Martinez", edat 23 i telefon 99999

  Escenari: Crear un val amb exit per un client soci
    Quan "Marc" "Martinez" 99999 vol fer una devolucio de productes
    I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol un val a les "12:30:59" amb el motiu "Devolucio de Optimus Prime i Primo Optimo"
    Aleshores hi ha una val de devolucio amb la quantitat 25€, fet en el dia "30-Nove-15", a l'hora "12:30:59", amb motiu "Devolucio de Optimus Prime i Primo Optimo", del soci "Marc" "Martinez" 99999, en tpv 1, pel venedor "Joan"


  Escenari: Crear un val amb exit per un client no soci
    Quan un client no soci vol fer una devolucio de productes
    I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol un val a les "12:30:59" amb el motiu "Devolucio de Optimus Prime i Primo Optimo"
    Aleshores hi ha una val de devolucio amb la quantitat 25€, fet en el dia "30-Nove-15", a l'hora "12:30:59", amb motiu "Devolucio de Optimus Prime i Primo Optimo", d'un client no soci, en tpv 1, pel venedor "Joan"

  Escenari: Crear un dues vals sense amb el mateix data, hora i tpv sense exit
    Quan "Marc" "Martinez" 99999 vol fer una devolucio de productes
    I vol retornar el producte amb nom "Optimus Prime" i codi de barres 1234567
    I vol un val a les "12:30:59" amb el motiu "Devolucio de Optimus Prime"
    I un client no soci vol fer una devolucio de productes
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol un val a les "12:30:59" amb el motiu "Devolucio de Primo Optimo"
    Aleshores obtinc un error que diu: "Ja existeix devolucio amb la data, hora i tpv donada"

  Escenari: Cobrar un val de devolucio amb exit
    Quan "Marc" "Martinez" 99999 vol fer una devolucio de productes
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol un val a les "12:30:59" amb el motiu "Devolucio de Primo Optimo"
    I que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I el client dona un val de devolucio del dia "30-Nove-15", hora "12:30:59" i tpv "1"
    I indico que el client ha entregat 30€ per a pagar en metàlic
    Aleshores el tiquet es
    """
    1: Optimus Prime ------ 23 * 1 unit(s) = 23€
    ----
    I.V.A:
    19€ + 21%: 23€
    ----
    Total sense descompte: 23€
    Val de devolucio: 2€
    Total amb descompte: 21€
    Entregat: 30€
    Canvi:9€
    Tipus de pagament: Metalic
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """

  Escenari: Cobrar dues vegades un mateix val de devolucio sense exit
    Quan "Marc" "Martinez" 99999 vol fer una devolucio de productes
    I vol retornar el producte amb nom "Primo Optimo" i codi de barres 7654321
    I vol un val a les "12:30:59" amb el motiu "Devolucio de Primo Optimo"
    I que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I el client dona un val de devolucio del dia "30-Nove-15", hora "12:30:59" i tpv "1"
    I el client dona un val de devolucio del dia "30-Nove-15", hora "12:30:59" i tpv "1"
    Aleshores obtinc un error que diu: "Val de devolucio usat"