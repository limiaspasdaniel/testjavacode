# language: ca

#noinspection SpellCheckingInspection
Característica: Llistat de vendes

  Rerefons: Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona"
    I que existeix el venedor "Joan" amb contrasenya "1234"
    I que existeix el venedor "Alba" amb contrasenya "2345"
    I que existeix el gestor "Justino" amb contrasenya "0000"
    I que l'usuari "Joan" amb contrasenya "1234" ha iniciat el torn al tpv
    I inicio una nova venta a la data "20-11-2015" i hora "11:00:00"
    I un producte amb nom "Gollum", preu 20€, iva 10% i codi de barres 2345671
    I un producte amb nom "Optimus Prime", preu 30€, iva 10% i codi de barres 1234567
    I que he afegit el producte de codi de barres 1234567 a la venta
    I que he afegit el producte de codi de barres 2345671 a la venta
    I indico que el client ha entregat 55€ per a pagar en metàlic
    I que en "Joan" ha fet LogOut
    I que l'usuari "Alba" amb contrasenya "2345" ha iniciat el torn al tpv
    I inicio una nova venta a la data "21-11-2015" i hora "17:30:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I indico que el client ha entregat 30€ per a pagar en metàlic
    I que en "Alba" ha fet LogOut

  Escenari: Historial de vendes
    Donat que l'usuari "Justino" amb contrasenya "0000" ha iniciat el torn al tpv
    Quan vull consultar l'historial de vendes
    Aleshores l'historial de vendes és
    """
    Venedor     | Data           | Hora       | Botiga       | Preu
    Joan          20-11-2015       11:00:00     Girona         50€
    Alba          21-11-2015       17:30:00     Girona         30€
    """