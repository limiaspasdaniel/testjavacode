# language: ca

#noinspection SpellCheckingInspection
Característica: Afegir producte a la venda

  Rerefons:
    Donat un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Gollum", preu 20€, iva 21% i codi de barres 2345671
    I un producte amb nom "Optimus Composite", preu 31€, iva 21% i codi de barres 0
    I que estem al tpv número 1 de la botiga "Girona 1"
    I que en "Joan" ha iniciat el torn al tpv
    I que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I línia de venta 1 és de 1 unitats de "Optimus Prime" a 23€ cada una per un total de 23€

  Escenari: Afegir un producte per codi de barres a la venda
    Quan afegeixo el producte de codi de barres 2345671 a la venta
    Aleshores la venta té 2 línies
    I línia de venta 2 és de 1 unitats de "Gollum" a 20€ cada una per un total de 20€
    I el total de la venta actual és de 43€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 1u = 23€
    Gollum - 20€/u x 1u = 20€
    ---
    Total: 43€
    """

  Escenari: Afegir un producte que ja está present a una línia de venta
    Quan afegeixo el producte de codi de barres 1234567 a la venta
    Aleshores la venta té 1 línia
    I línia de venta 1 és de 2 unitats de "Optimus Prime" a 23€ cada una per un total de 46€
    I el total de la venta actual és de 46€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 2u = 46€
    ---
    Total: 46€
    """

  Escenari: Cercar productes a través d'un fragment del seu nom
    Quan introdueixo "Optimus" al cercador de productes
    Aleshores la pantalla mostra
    """
    S'han trobat 2 productes amb aquest nom:
    ---
    1: Optimus Composite - Preu: 31€ - Codi de barres: 0
    2: Optimus Prime - Preu: 23€ - Codi de barres: 1234567
    """

  Escenari: Afegir un producte sense codi de barres a la venda
    Quan afegeixo el producte de nom "Optimus Composite" a la venta
    Aleshores la venta té 2 línies
    I línia de venta 2 és de 1 unitats de "Optimus Composite" a 31€ cada una per un total de 31€
    I el total de la venta actual és de 54€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 1u = 23€
    Optimus Composite - 31€/u x 1u = 31€
    ---
    Total: 54€
    """

  Escenari: Afegir un producte amb descompte X per Y associat a la venda
    Donat que existeix un descompte de 2x1 amb nom "2x1 en Gollum" assignat al producte amb nom "Gollum"
    Quan afegeixo el producte de codi de barres 2345671 a la venta
    I afegeixo el producte de codi de barres 2345671 a la venta
    I afegeixo el producte de codi de barres 2345671 a la venta
    I afegeixo el producte de codi de barres 2345671 a la venta
    I afegeixo el producte de codi de barres 2345671 a la venta
    Aleshores la venta té 2 línies
    I línia de venta 2 és de 5 unitats de "Gollum" a 20€ cada una per un total de 100€
    I el total de la venta actual és de 83€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 1u = 23€
    Gollum - 20€/u x 5u = 100€
        Descompte: 2x1 en Gollum = -40€
    ---
    Total: 83€
    """

  Escenari: Afegir un producte amb descompte de percentatge associat a la venda
    Donat que existeix un descompte del 10% amb nom "-10% en Gollum" assignat al producte amb nom "Gollum"
    Quan afegeixo el producte de codi de barres 2345671 a la venta
    Aleshores la venta té 2 línies
    I línia de venta 2 és de 1 unitats de "Gollum" a 20€ cada una per un total de 20€
    I el total de la venta actual és de 41€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 23€/u x 1u = 23€
    Gollum - 20€/u x 1u = 20€
        Descompte: -10% en Gollum = -2€
    ---
    Total: 41€
    """