# language: ca

#noinspection SpellCheckingInspection
Característica: Cobrar una venda pagada amb targeta

  Rerefons:
    Donat un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I que estem al tpv número 1 de la botiga "Girona 1"
    I que en "Joan" ha iniciat el torn al tpv

  Escenari: Cobrar una venta pagada amb targeta sense pin
    Donat que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I el total de la venta actual és de 23€
    Quan indico que el client pagarà amb targeta i la passo pel lector, que rep el codi de targeta 123456789
    Aleshores el tpv mostra al client
    """
    Operació realitzada correctament. Import pagat: 23€
    """
    I el tiquet es
    """
    1: Optimus Prime ------ 23 * 1 unit(s) = 23€
    ----
    I.V.A:
    18€ + 21%: 23€
    ----
    Total sense descompte: 23€
    Total amb descompte: 23€
    Entregat: 23€
    Canvi:0€
    Tipus de pagament: Targeta
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """

  Escenari: Cobrar una venta pagada amb targeta sense fons
    Donat que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I el total de la venta actual és de 23€
    Quan indico que el client pagarà amb targeta i la passo pel lector, que rep el codi de targeta 123456788
    Aleshores el tpv mostra al client
    """
    Targeta sense fons. Operació no realitzada.
    """
  Escenari: Intentar cobrar una venta pagada amb targeta amb pin
    Donat que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I el total de la venta actual és de 23€
    Quan indico que el client pagarà amb targeta i la passo pel lector, que rep el codi de targeta 987654321
    Aleshores el tpv mostra al client
    """
    Introdueixi el seu pin, si us plau.
    """

  Escenari: Cobrar una venta pagada amb targeta amb pin
    Donat que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I el total de la venta actual és de 23€
    I el client vol pagar amb la targeta amb pin de codi 987654321
    Quan el client introdueix el pin 2468
    Aleshores el tpv mostra al client
    """
    Operació realitzada correctament. Import pagat: 23€
    """
    I el tiquet es
    """
    1: Optimus Prime ------ 23 * 1 unit(s) = 23€
    ----
    I.V.A:
    18€ + 21%: 23€
    ----
    Total sense descompte: 23€
    Total amb descompte: 23€
    Entregat: 23€
    Canvi:0€
    Tipus de pagament: Targeta
    Gestor: Joan
    Num de TPV: 1
    Botiga: Girona
    Data: 11:00:00 20-11-2015
    """

  Escenari: Error si el client introdueix un pin incorrecte
    Donat que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    I que he afegit el producte de codi de barres 1234567 a la venta
    I el total de la venta actual és de 23€
    I el client vol pagar amb la targeta amb pin de codi 987654321
    Quan el client introdueix el pin 2467
    Aleshores el tpv mostra al client
    """
    Pin incorrecte. Operació no realitzada.
    """

  Escenari: Error si intentem cobrar una venta sense productes
    Donat que hi ha una venta iniciada a la data "20-11-2015" i hora "11:00:00"
    Quan indico que el client pagarà amb targeta i la passo pel lector, que rep el codi de targeta 987654321
    Aleshores obtinc un error que diu: "No es pot cobrar una venta sense cap producte"

  Escenari: Error si intentem cobrar una venta no iniciada
    Quan indico que el client pagarà amb targeta i la passo pel lector, que rep el codi de targeta 987654321
    Aleshores obtinc un error que diu: "No es pot cobrar una venta si no està iniciada"