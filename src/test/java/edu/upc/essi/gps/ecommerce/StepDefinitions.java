package edu.upc.essi.gps.ecommerce;


import cucumber.api.PendingException;
import cucumber.api.java.ca.Aleshores;
import cucumber.api.java.ca.Donat;
import cucumber.api.java.ca.I;
import cucumber.api.java.ca.Quan;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class StepDefinitions {

    private ProductsService productsService = new ProductsService(new ProductsRepository());
    private DescomptesService descomptesService = new DescomptesService(new DescomptesRepository());
    private Exception exception;
    private PosController posController;
    private int change;
    private int barCode;
    private String tiquet;
    private Soci sociAConsultar;
    private String msgQuadraments;
    private String HistorialVentes;
    private String msgDevolucions;
    private String logutmsg;
    private int money = 0;
    private int pts = 0;
    private String msgVenedors;
    
    public void tryCatch(Runnable r) {
        try {
            r.run();
            this.exception = null;
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Aleshores("^obtinc un error que diu: \"([^\"]*)\"$")
    public void checkErrorMessage(String msg) throws Throwable {
        assertNotNull(this.exception);
        assertEquals(msg, this.exception.getMessage());
    }


    @Donat("^que estem al tpv número (\\d+) de la botiga \"([^\"]*)\"$")
    public void setupPos(int posNumber, String shop) throws Throwable {
        this.posController = new PosController(shop, posNumber, productsService, descomptesService);
    }

    @Aleshores("^el tpv està en ús per en \"([^\"]*)\"$")
    public void checkCurrentSaleAssistantName(String saleAssistantName) throws Throwable {
        assertEquals(saleAssistantName, this.posController.getCurrentSaleAssistantName());
    }

    @Aleshores("ningú està utilitzant el tpv")
    public void checkCurrentSaleAssistantName() throws Throwable {
        assertNull(this.posController.getCurrentSaleAssistantName());
    }

    @Aleshores("^la venta actual és de'n \"([^\"]*)\" al tpv (\\d+) de la botiga \"([^\"]*)\"$")
    public void checkCurrentSaleData(String saleAssistant, int posNumber, String shop) throws Throwable {
        Sale s = this.posController.getCurrentSale();
        assertNotNull(s);
        assertEquals(shop, s.getShop());
        assertEquals(posNumber, s.getPosNumber());
        assertEquals(saleAssistant, s.getSaleAssistantName());
    }


    @Quan("^que en \"([^\"]*)\" ha fet una venta de \"([^\"]*)\" el \"([^\"]*)\" a les \"([^\"]*)\" a la botiga \"([^\"]*)\"$")
    public void VenedorFaVenta(String venedor, String preu, String dia, String hora, String botiga) throws Throwable {
        tryCatch(() -> this.posController.doSale(venedor, preu, dia, hora, botiga));
    }

    @Quan("^inicio el torn al tpv com a \"([^\"]*)\"$")
    public void login(String saleAssistantName) throws Throwable {
        tryCatch(() -> this.posController.login(saleAssistantName));
    }

    @Quan("^inicio el torn al tpv com a \"([^\"]*)\" amb contrasenya \"([^\"]*)\"$")
    public void login(String saleAssistantName, String saleAssistantPassword) throws Throwable {
        tryCatch(() -> this.posController.login(saleAssistantName, saleAssistantPassword));
    }

    @Quan("^que en \"([^\"]*)\" ha fet LogOut$")
    public void logout(String venedor) throws Throwable {
        tryCatch(() -> this.posController.logout());
    }

    @Donat("^que existeix el venedor \"([^\"]*)\" amb contrasenya \"([^\"]*)\"$")
    public void exist_venedor(String saleAssistantName, String saleAssistantPassword) throws Throwable {
        this.posController.login("admin", "1234");
        this.posController.newUser(Usuari.Tipus.VENEDOR, saleAssistantName, saleAssistantPassword);
        this.posController.logout();
    }

    @Donat("^que existeix el gestor \"([^\"]*)\" amb contrasenya \"([^\"]*)\"$")
    public void exist_gestor(String saleAssistantName, String saleAssistantPassword) throws Throwable {
        this.posController.login("admin", "1234");
        this.posController.newUser(Usuari.Tipus.GESTOR, saleAssistantName, saleAssistantPassword);
        this.posController.logout();
    }

    @Donat("^que en \"([^\"]*)\" ha iniciat el torn al tpv$")
    public void hasLoggedIn(String saleAssistantName) throws Throwable {
        this.posController.login(saleAssistantName);
    }

    @Donat("^la data es \"([^\"]*)\"$")
    public void dataObertura(String data) {
        this.posController.setData(data);
    }

    @Donat("^que l'usuari \"([^\"]*)\" amb contrasenya \"([^\"]*)\" ha iniciat el torn al tpv$")
    public void hasLoggedIn(String saleAssistantName, String saleAssistantPassword) throws Throwable {
        this.posController.login(saleAssistantName, saleAssistantPassword);
    }

    @Quan("^faig LogOut del tpv$")
    public void faig_LogOut_del_tpv() throws Throwable {
        tryCatch(() -> this.posController.logout());
    }

    @Quan("^inicio una nova venta$")
    public void tryStartSale() throws Throwable {
        tryCatch(() -> this.posController.startSale());
    }

    @Quan("^inicio una nova venta a la data \"([^\"]*)\" i hora \"([^\"]*)\"$")
    public void tryStartSale(String data, String hora) throws Throwable {
         tryCatch(() -> this.posController.startSale(data, hora));

    }

    @Donat("^que hi ha una venta iniciada a la data \"([^\"]*)\" i hora \"([^\"]*)\"$")
    public void saleStarted(String data, String hora) throws Throwable {
        this.posController.startSale(data, hora);
    }

    @Donat("^un producte amb nom \"([^\"]*)\", preu (\\d+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void productCreated(String productName, int price, int vatPct, int barCode) throws Throwable {
        this.productsService.newProduct(productName, price, vatPct, barCode);
    }

    @Quan("^tenim un producte amb nom \"([^\"]*)\", preu (\\d+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void productNewCreated(String productName, int price, int vatPct, int barCode) throws Throwable {
        this.barCode = barCode;
        tryCatch(() -> this.productsService.newProduct(productName, price, vatPct, barCode));
    }

    @Quan("^tenim un producte amb nom \"([^\"]*)\", preu -(\\d+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void productNewCreatedNegativePrice(String productName, int price, int vatPct, int barCode) throws Throwable {
        tryCatch(() -> this.productsService.newProduct(productName, -price, vatPct, barCode));
    }

    @Quan("^tenim un producte amb nom \"([^\"]*)\", preu (\\d+)€, iva -(\\d+)% i codi de barres (\\d+)$")
    public void productNewCreatedNegativeIVA(String productName, int price, int vatPct, int barCode) throws Throwable {
        tryCatch(() -> this.productsService.newProduct(productName, price, -vatPct, barCode));
    }

    @Quan("^tenim un producte amb nom \"([^\"]*)\", preu (\\d+)€, iva (\\d+)% i codi de barres -(\\d+)$")
    public void productCreatedIncorrectBarCode(String productName, int price, int vatPct, int barCode) throws Throwable {
        tryCatch(() -> this.productsService.newProduct(productName, price, vatPct, -barCode));
    }

    @Aleshores("^el sistema indica que el producte amb codi barres \"([^\"]*)\" s'ha afegit correctament$")
    public void el_sistema_indica_que_el_producte_amb_codi_barres_s_ha_afegit_correctament(String codi) throws Throwable {
        String aux = this.barCode+"";
        assertEquals(aux, codi);
    }

    @Quan("^afegeixo el producte de codi de barres (\\d+) a la venta$")
    public void addProductByBarCode(int barCode) throws Throwable {
        this.posController.addProductByBarCode(barCode);
    }

    @Donat("^que he afegit el producte de codi de barres (\\d+) a la venta$")
    public void productByBarCodeAdded(int barCode) throws Throwable {
        this.posController.addProductByBarCode(barCode);
    }


    @Aleshores("^la venta té (\\d+) (?:línia|línies)$")
    public void la_venta_té_n_linies(int expectedNumberOfLines) throws Throwable {
        assertEquals(expectedNumberOfLines, this.posController.getCurrentSale().getLines().size());
    }

    @Aleshores("^línia de venta (\\d+) és de (\\d+) unitats de \"([^\"]*)\" a (\\d+)€ cada una per un total de (\\d+)€$")
    public void línia_de_venta_és_de_unitats_de_a_€_cada_una_per_un_total_de_€(int lineNumber, int units, String productName, int unitPrice, int totalPrice) throws Throwable {
        SaleLine sl = this.posController.getCurrentSale().getLines().get(lineNumber - 1);
        assertEquals(units, sl.getAmount());
        assertEquals(unitPrice, sl.getUnitPrice());
        assertEquals(totalPrice, sl.getAmount()*sl.getUnitPrice());
        assertEquals(productName, sl.getProductName());
    }

    @Aleshores("^el total de la venta actual és de (\\d+)€$")
    public void el_total_de_la_venta_actual_és_de_€(int saleTotal) throws Throwable {
        assertEquals(saleTotal, this.posController.getCurrentSale().getPreuTotal());
    }


    @Aleshores("^la pantalla del client del tpv mostra$")
    public void la_pantalla_del_client_del_tpv_mostra(String msg) throws Throwable {
        assertEquals(msg, this.posController.getCustomerScreenMessage());
    }

    @Aleshores("^la pantalla del tpv mostra$")
    public void la_pantalla_del_client_fer_descompte(String msg) throws Throwable {
        assertEquals(msg, this.posController.getPantallaFerDescs());
    }

    @Quan("^indico que el client ha entregat (\\d+)€ per a pagar en metàlic$")
    public void cashPayment(int delivered) throws Throwable {
        tryCatch(() -> this.change = this.posController.cashPayment(delivered));
    }

    @Quan("^indico que el client ha pagat (\\d+)€ amb targeta$")
    public void cardPayment(int delivered) throws Throwable {
        try {
            this.change = this.posController.cardPayment(delivered);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    @Donat("^que el client ha pagat (\\d+)€ en metàlic$")
    public void clientPaid(int delivered) throws Throwable {
        change = posController.cashPayment(delivered);
    }

    @Aleshores("^el tpv m'indica que el canvi a retornar és de (\\d+)€$")
    public void checkChange(int expectedChange) throws Throwable {
        assertEquals(expectedChange, change);
    }


    @Quan("^imprimeixo el ticket de la venda$")
    public void imprimeixo_el_tiquet_de_la_venda(String msg) throws Throwable {
        // Express the Regexp above with the code you wish you had
        tiquet = posController.imprimirTicket();
        assertEquals(msg, posController.imprimirTicket());
    }

    @Aleshores("^el tiquet es$")
    public void el_tiquet_es(String msg) throws Throwable {
        //assertEquals(msg, tiquet);
    }

    /*@Quan("^indico que el client s'ha entregat (\\d+)€ per a pagar en metàlic$")
    public void shaEntregat(int entregat) throws Throwable {
        posController.setEntregatCanviMetalic(entregat);
    }*/

    @Quan("^indico que el client s'ha entregat (\\d+)€ per a pagar amb target")
    public void shaEntregatTargeta(int entregat) throws Throwable {
        posController.setEntregatTargeta(entregat);
    }

    //INICIO zona consultar socis

    @Donat("^hi ha una gestio de socis$")
    public void iniciarGestioSocis() throws Throwable {
        this.posController.iniciarGestioSocis();
    }

    @Donat("^un soci amb nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+) i telefon (\\d+)$")
    public void crearSoci(String nom, String cognom, int edat, int telefon) {
        this.posController.crearSoci(nom, cognom, edat, telefon);
    }

    @Donat("^\"([^\"]*)\" \"([^\"]*)\" (\\d+) fa una compra a la data \"([^\"]*)\" i hora \"([^\"]*)\"$")
    public void afegirCompra(String nom, String cognom, int telefon, String data, String hora) {
        this.posController.afegirCompra(nom, cognom, telefon, data, hora);
    }

    @Quan("^el gestor vol veure tots els socis$")
    public void veureSocis() {

    }

    @Aleshores("^hi haura/n (\\d+) socis$")
    public void hi_hauran_socis(int nsocis) {
        assertEquals(nsocis, this.posController.getNSocis());
    }

    @Aleshores("^hi ha un soci amb nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+), telefon (\\d+) i (\\d+) punts$")
    public void hi_ha_un_soci_amb_nom_cognom_edat_tel_punts(String nom, String cognom, int edat, int telefon, int punts) {
        assertTrue("Soci no trobat", this.posController.existSociAmb(nom, cognom, edat, telefon, punts));
    }

    @Aleshores("^sobre la pantalla mostra tots els socis$")
    public void mostraTotsSocis(String missatge) {
        assertEquals(missatge, this.posController.getMissatgeSocis());
    }

    @Quan("^el gestor vol veure detalls del soci \"([^\"]*)\" \"([^\"]*)\" (\\d+)$")
    public void el_gestor_vol_veure_detalls_d_un_soci(String nom, String cognom, int telefon) {
        this.sociAConsultar = this.posController.getSoci(nom, cognom, telefon);
    }

    @Aleshores("^sobre la pantalla mostra el soci seleccionat detallat")
    public void mostraSociDetallat(String missatge) {
        assertNotNull("No ha seleccionat un soci", sociAConsultar);
        assertEquals(missatge, this.posController.getMissatgeDetallsSoci(this.sociAConsultar, missatge));
    }

    //FIN zona consultar socis

    /**
     * Introduir quadrament
     **/

    @Donat("^que el recompte inicial del dia és (\\d+)€ a dia \"([^\"]*)\"$")
    public void creaQuadrament(float imInicial, String dia) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date data = sdf.parse(dia);
        posController.setCurrentDate(data);
        posController.creaQuadrament(imInicial);
    }

    @Quan("^introdueix l'efectiu de la caixa al final del dia que és (\\d+)€$")
    public void introduirEfectiuReal(float imIntroduit) {
        posController.introduirEfectiuFinalDia(imIntroduit);
    }

    @Quan("^introdueix l'efectiu de la caixa al final del dia que és \"([^\"]*)\"€$")
    public void introduirEfectiuReal(String imIntroduit) {
        float f = Float.parseFloat(imIntroduit);
        posController.introduirEfectiuFinalDia(f);
    }

    @Quan("^el venedor decideix tancar el quadrament tot i ser incorrecte$")
    public void tancarQuadramentIncorrecte(){
        posController.tancarQuadramentIncorrecte();
    }

    @Aleshores("^la pantalla mostra un missatge que diu: \"([^\"]*)\"$")
    public void mostraMissatge(String msg) throws Throwable {
        assertEquals(msg, posController.getMsgQuadrament());
    }

    @Aleshores("^El quadrament queda enregistrat al sistema$")
    public void comprovaQuadramentAfegit(){
        assertNotEquals(null, posController.getMostRecentQuadrament());
    }

    /**End introduir quadrament**/

    @Quan("^afegeixo el producte de nom \"([^\"]*)\" a la venta$")
    public void addProductByName(String name) throws Throwable {
        this.posController.addProductByName(name);
    }


    /*VISUALITZAR QUADRAMENTS*/


    @Quan("^visualitzo els quadraments$")
    public void visualitzo_els_quadraments() throws Throwable {
        msgQuadraments = posController.visualitzarQuadraments();
        System.out.println(msgQuadraments);
    }

    @Aleshores("^aquests són \"([^\"]*)\"$")
    public void aquests_són(String msg) throws Throwable {
        assertEquals(msg, msgQuadraments);
    }


    @I("^hi ha dos quadraments, el primer amb una diferencia de (\\d+)€, amb tpv numero (\\d+), i es el torn del venedor amb dni \"([^\"]*)\" i el segon amb una diferencia de (\\d+)€, tpv numero (\\d+) i es el torn del venedor amb dni \"([^\"]*)\"$")
    public void crearQuadraments(int diferencia1, int tpv1, String dni1, int diferencia2, int tpv2, String dni2) throws Throwable {
        Quadrament quadrament1 = new Quadrament();
        Quadrament quadrament2 = new Quadrament();

        quadrament1.setDiferencia(diferencia1);
        quadrament2.setDiferencia(diferencia2);
        quadrament1.setTpv(tpv1);
        quadrament2.setTpv(tpv2);
        quadrament1.setDniVenedor(dni1);
        quadrament2.setDniVenedor(dni2);

        String str_date = "11-June-07";
        DateFormat formatter;
        Date date, date1;
        formatter = new SimpleDateFormat("dd-MMMM-yy");
        date = formatter.parse(str_date);
        str_date = "11-June-08";
        date1 = formatter.parse(str_date);

        quadrament1.setData(date);
        quadrament2.setData(date1);

        posController.afegirQuadrament(quadrament1);
        posController.afegirQuadrament(quadrament2);

    }

    //INICIO zona crear val devolucio
    @Donat("^hi ha una gestio de vals de devolucions$")
    public void iniciarGestioValsDevolucio() {
        this.posController.iniciarGestioValsDevolucio();
    }
    @Quan("^\"([^\"]*)\" \"([^\"]*)\" (\\d+) vol fer una devolucio de productes$")
    public void sociVolFerDevolucio(String nom, String cognom, int telefon) {
        this.posController.iniciarDevolucioAmbSoci(nom, cognom, telefon);
    }

    @Quan("^vol retornar el producte amb nom \"([^\"]*)\" i codi de barres (\\d+)$")
    public void retornarProducte(String nomProducte, int codiProducte) {
        this.posController.afegirProducteDevolucio(nomProducte, codiProducte);
    }

    @Quan("^vol un val a les \"([^\"]*)\" amb el motiu \"([^\"]*)\"$")
    public void vol_un_val_a_les_hores_minuts_i_segons_amb_el_motiu(String temps, String motiu) throws Throwable {
        tryCatch(() -> this.posController.afegirValDevolucio(temps, motiu));
        tryCatch(() -> this.posController.afegirDevolucio(temps, motiu));
    }

    @Quan("^no vol val a les \"([^\"]*)\" amb el motiu \"([^\"]*)\"$")
    public void no_vol_val_a_les_hores_minuts_i_segons_amb_el_motiu(String temps, String motiu) throws Throwable {
        tryCatch(() -> this.posController.afegirDevolucio(temps, motiu));
    }

    @Aleshores("^hi ha una val de devolucio amb la quantitat (\\d+)€, fet en el dia \"([^\"]*)\", a l'hora \"([^\"]*)\", amb motiu \"([^\"]*)\", del soci \"([^\"]*)\" \"([^\"]*)\" (\\d+), en tpv (\\d+), pel venedor \"([^\"]*)\"$")
    public void comprovarValDevolucioAmbSoci(int quantitat, String data, String hora, String motiu, String nomSoci, String cognomSoci, int telSoci, int posNumber, String nomVenedor) {
        ValDevolucio val = this.posController.getValDevolucio(data, hora, posNumber);
        assertEquals(quantitat, val.getQuantitat());
        assertEquals(data, val.getDataDevolucio());
        assertEquals(hora, val.getHoraDevolucio());
        assertEquals(motiu, val.getMotiu());
        assertEquals(nomSoci, val.getSoci().getNom());
        assertEquals(cognomSoci, val.getSoci().getCognom());
        assertEquals(telSoci, val.getSoci().getTel());
        assertEquals(posNumber, val.getPosNumber());
        assertEquals(nomVenedor, val.getSaleAssistantName());
    }

    @Quan("^un client no soci vol fer una devolucio de productes$")
    public void un_client_no_soci_vol_fer_una_devolucio_de_productes() throws Throwable {
        tryCatch(() -> this.posController.iniciarDevolucioNoSoci());
    }

    @Aleshores("^hi ha una val de devolucio amb la quantitat (\\d+)€, fet en el dia \"([^\"]*)\", a l'hora \"([^\"]*)\", amb motiu \"([^\"]*)\", d'un client no soci, en tpv (\\d+), pel venedor \"([^\"]*)\"$")
    public void comprovarValDevolucioNoSoci(int quantitat, String data, String hora, String motiu, int posNumber, String nomVenedor){
        ValDevolucio val = this.posController.getValDevolucio(data, hora, posNumber);
        assertEquals(quantitat, val.getQuantitat());
        assertEquals(data, val.getDataDevolucio());
        assertEquals(hora, val.getHoraDevolucio());
        assertEquals(motiu, val.getMotiu());
        assertEquals(posNumber, val.getPosNumber());
        assertEquals(nomVenedor, val.getSaleAssistantName());
        assertNull(val.getSoci());
    }

    @Quan("^el client dona un val de devolucio del dia \"([^\"]*)\", hora \"([^\"]*)\" i tpv \"([^\"]*)\"$")
    public void introduirValDevolucio (String data, String hora, int posNumber) throws Throwable {
        tryCatch(() -> this.posController.usarValDevolucio(data, hora, posNumber));
    }


    @Aleshores("^aixo esta be$")
    public void aixo_esta_be() {
        System.out.print(msgDevolucions);
    }

    //FIN zona crear val devolucio

    //INICI realitzar devolucio
    @Quan ("^vol veure les devolucions$")
    public void consultarDevolucions () {
        this.msgDevolucions = this.posController.consultarDevolucions();
    }

    @Donat("^durant el dia es realitzen vendes, generacio automatica: 230€$")
    public void generar_ventes_automatic() {
        String data = "30-11-2015";
        String hora = "11:00:00";
        int codiBarra = 1234567;
        this.posController.startSale(data, hora);
        for (int i = 0; i < 10; i++) {
            this.posController.addProductByBarCode(codiBarra);
        }
        tryCatch(() -> this.change = this.posController.cashPayment(500));
    }

    @Aleshores("^a la pantalla mostra un llistat de tots els devolucions$")
    public void comparaMsgDevolucio(String msg) {
        assertEquals(msg, this.msgDevolucions);
    }

    @Aleshores("^el quadrament del dia es (\\d+)€")
    public void quadramentDelDia (int quadrament) {
        assertTrue(this.posController.mateixQuadramentActual(quadrament));
    }
    //FIN realitzar devolucio

    //Resum Ventes
    @Quan("^vull consultar l'historial de vendes$")
    public void consultaVendes() throws Throwable {
        tryCatch(() -> HistorialVentes = posController.obteHistorialVentes());
    }

    @Aleshores("^l'historial de vendes és$")
    public void historialEs(String m) throws Throwable {
        assertEquals(m, this.posController.obteHistorialVentes());
    }

    //INI Fer descomptes en certs products

    @Quan("^he afegit el producte de codi de barres (\\d+) a la venta amb (\\d+) unitat$")
    public void productByBarCodeAddedWithUnit(int barCode, int unit) throws Throwable {
        this.posController.addProductByBarCodeWithUnit(barCode, unit);
    }

    @Donat("^que el producte nom \"([^\"]*)\", preu (\\d+)€, iva (\\d+)% i codi de barres (\\d+) ja existeix$")
    public void newProduct(String name, int price, int vatPct, int barCode) throws Throwable
    {
        tryCatch(() -> this.posController.newPctExisteix(name, price, vatPct, barCode));
    }

    @Quan("^es dona d'alta un descompte amb percentatge amb nom \"([^\"]*)\" i percentatge (\\d+)$")
    public void ferPercDescompte(String name, int perc) throws Throwable {
        tryCatch(() -> posController.newPercDescompte(name, perc));
    }

    /*@Quan("^comprovat que el producte codi de barres (\\d+) no te cap descompte$")
    public Boolean comprovatDescompte(int barCode) throws Throwable {
        tryCatch(() -> posController.checkDescompte(barCode));
    }*/

    @Quan("^es dona d'alta un descompte de X per Y amb nom \"([^\"]*)\", X: (\\d+) i Y: (\\d+)$")
    public void ferXYDescompte(String name, int x, int y) throws Throwable {
        tryCatch(() -> posController.newXYDescompte(name, x, y));
    }

    @Quan("^es dona d'alta un descompte de val amb nom \"([^\"]*)\" i val: (\\d+)$")
    public void ferValDescompte(String name, int val) throws Throwable {
        tryCatch(() -> posController.newValDescompte(name, val));
    }

    @Quan("^s'assigna al producte amb nom \"([^\"]*)\" el descompte amb nom \"([^\"]*)\"$")
    public void assignPercPct(String pctName, String nomDescomp)
    {
        tryCatch(() -> posController.assignPerProduct(pctName, nomDescomp));
    }

    @Aleshores("^el descompte \"([^\"]*)\" existeix a la llistat de descomptes$")
    public void existsDescompte(String name) throws Throwable {
        Descompte des = posController.findDescompteByName(name);
        assertEquals(name, des.getName());
    }

    @Aleshores("^el producte codi de barres (\\d+) té assignat el descompte \"([^\"]*)\"$")
    public void teAssignatDescompPct(int codeBar, String nomD) throws Throwable {
        Product pct = this.posController.getProductsService().findByBarCode(codeBar);
        assertEquals(pct.getNomDescomp(), nomD);
    }

    /*@Aleshores("^la llista de productes amb el descompte nom \"([^\"]*)\" es$")
    public void llistaProductDescomp(String nameDescomp, String msg) throws Throwable
    {
        Descompte des = posController.findDescompteByName(nameDescomp);
        assertEquals(msg, posController.listPctInfo(des));
    }*/

    //FIN Fer descomptes en certs products

    //INICI Afegir producte a la venda
    @Quan("^introdueixo \"([^\"]*)\" al cercador de productes$")
    public void searchProductsByName(String name) throws Throwable {
        this.posController.searchProductsByName(name);
        List<Product> lP = this.posController.getProductesCercatsPerNom();
        for (Product p : lP) {
            assertTrue(p.getName().contains(name));
        }
    }

    @Aleshores("^la pantalla mostra$")
    public void la_pantalla_mostra(String m) throws Throwable {
        assertEquals(m, this.posController.getSaleAssistantScreenMessage());
    }


    //FINAL AFEGIR PRODUCTE A VENDA

    //INICI Introduir val descompte a una venda
    @Donat ("^un val de descompte d'import total de (\\d+)€ amb nom \"([^\"]*)\", codi (\\d+), compra mínima (\\d+)€, \"([^\"]*)\" i que caduca el \"([^\"]*)\"$")
    public void crearVDescompteImp(int impDesc, String nom, int codi, int cmin, String us, String cad) throws Throwable {
        this.posController.crearValDescompteTot(codi, nom, "import total", cad, cmin, impDesc);
        if (us.equals("usat")) this.posController.usarValDescompte(codi);
    }

    @Donat ("^un val de descompte de percentatge total del (\\d+)% amb nom \"([^\"]*)\", codi (\\d+), compra mínima (\\d+)€, \"([^\"]*)\" i que caduca el \"([^\"]*)\"$")
    public void crearVDescomptePerc(int percDesc, String nom, int codi, int cmin, String us, String cad) throws Throwable {
        this.posController.crearValDescompteTot(codi, nom, "percentatge total", cad, cmin, percDesc);
        if (us.equals("usat")) this.posController.usarValDescompte(codi);
    }

    @Donat ("^un val de descompte de (\\d+)x(\\d+) en \"([^\"]*)\" amb nom \"([^\"]*)\", codi (\\d+), \"([^\"]*)\" i que caduca el \"([^\"]*)\"$")
    public void crearVDescompteXxY(int pTot, int pPag, String prod, String nom, int codi, String us, String cad) throws Throwable {
        this.posController.crearValDescompteXxY(codi, nom, "XxY", cad, prod, pTot, pPag);
        if (us.equals("usat")) this.posController.usarValDescompte(codi);
    }

    @Donat ("^que he afegit el val de descompte amb codi (\\d+) a la venda$")
    public void afegirDesVal(int codi) {
        this.posController.afegirValDescompte(codi);
    }

    @Quan ("^afegeixo el val de descompte amb codi (\\d+) a la venda$")
    public void afegirDescompteVal(int codi) throws Throwable {
        tryCatch(() -> this.posController.afegirValDescompte(codi));
    }

    @Aleshores("^el val de descompte de codi (\\d+) està aplicat a la venda amb import descomptat (\\d+)€$")
    public void getDescompteVenda(int codi, int importDesc) {
        ValDescompte vd = this.posController.getCurrentSale().getValDescompteVenda();
        int descomptat = this.posController.getCurrentSale().getDescomptatVals();
        assertEquals(codi, vd.getCodi());
        assertEquals(importDesc, descomptat);
    }

    @Aleshores("^el val de descompte amb codi (\\d+) està \"([^\"]*)\"$")
    public void valUsat(int codi, String us) {
        assertTrue(this.posController.findValDescompte(codi).isUsat());
    }

    @Donat("^que existeix un descompte del (\\d+)% amb nom \"([^\"]*)\" assignat al producte amb nom \"([^\"]*)\"$")
    public void creaDescXxY(int perc, String nomDesc, String pctName) {
        posController.newPercDescompte(nomDesc, perc);
        posController.assignPerProduct(pctName, nomDesc);
    }

    @Donat("^que existeix un descompte de (\\d+)x(\\d+) amb nom \"([^\"]*)\" assignat al producte amb nom \"([^\"]*)\"$")
    public void creaDescPerc(int x, int y, String nomDesc, String pctName) {
        posController.newXYDescompte(nomDesc, x, y);
        posController.assignPerProduct(pctName, nomDesc);
    }
    //FINAL INTRODUIR DESCOMPTE A VENDA

    //INICI CREAR SOCI
    @Donat("^que hi ha al sistema un soci amb nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+) i telèfon (\\d+)")
    public void creaSoci(String nomSoci, String cognomSoci, int edatSoci, int telSoci) {
        posController.crearSoci(nomSoci, cognomSoci, edatSoci, telSoci);
    }
    @Quan ("^introdueixo les dades nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+) i telèfon (\\d+)")
    public void createSoci(String nomSoci, String cognomSoci, int edatSoci, int telSoci) throws Throwable {
        tryCatch(() -> posController.crearSoci(nomSoci, cognomSoci, edatSoci, telSoci));
    }

    @Aleshores("^hi ha un soci amb nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+), telèfon (\\d+) i (\\d+) punts")
    public void checkCurrentSoci(String nomSoci, String cognomSoci, int edatSoci, int telSoci, int punts) throws Throwable {
        assertTrue(this.posController.existSociAmb(nomSoci, cognomSoci, edatSoci, telSoci, 0));
    }

    //INICI PAGAMENT TARGETA
    @Donat("^el client vol pagar amb la targeta amb pin de codi (\\d+)$")
    public void pinCardPayment(int cardCode) throws Throwable {
        this.posController.setActualCardCode(cardCode);
    }

    @Quan("^indico que el client pagarà amb targeta i la passo pel lector, que rep el codi de targeta (\\d+)$")
    public void CardPayment(int cardCode) throws Throwable {
        tryCatch(() -> this.posController.cardPayMode(cardCode, -1));
    }

    @Quan ("^el client introdueix el pin (\\d+)$")
    public void pinEntry(int pinCode) throws Throwable {
        tryCatch(() -> this.posController.cardPayMode(-1, pinCode));
    }

    @Aleshores("^el tpv mostra al client$")
    public void showCust (String m) {
        assertEquals(m, this.posController.getMissatgePagamentTargeta());
    }
    //FINAL PAGAMENT TARGETA

    //INICI GESTIO VENEDORS
    @Donat("^que hi ha al sistema un venedor amb dni \"([^\"]*)\" i password \"([^\"]*)\"")
    public void crearVenedorInici(String dni, String password) {
        this.posController.newUser(Usuari.Tipus.VENEDOR, dni, password);
    }

    @Quan("^introdueixo les dades nom \"([^\"]*)\" i password \"([^\"]*)\"$")
    public void crearVenedor(String dni, String password) throws Throwable {
        tryCatch(() -> this.posController.newUser(Usuari.Tipus.VENEDOR, dni, password));
    }


    @Aleshores("^hi ha un venedor amb nom \"([^\"]*)\" i password \"([^\"]*)\"$")
    public void existVenedor(String dni, String password) throws Throwable {
        assertTrue(this.posController.existVenedor(dni, password));
    }

    @Quan("^visualitzo el llistat de tots els venedors del sistema$")
    public void showVenedors() {
        this.posController.getMissatgeInfoVenedors();
    }

    @Aleshores("^obtinc un missatge que diu: \"([^\"]*)\"$")
    public void checkLlistatVenedors(String veneds) throws Throwable {
        assertEquals(veneds, this.posController.getMissatgeInfoVenedors());
    }

    //Descanviar
    @Donat("^en el sistema hi ha un soci amb nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+), telèfon (\\d+) i (\\d+) punts$")
    public void setPtsSoci(String nom, String cognom, int edat, int tel, int pts)
    {
        Soci s = this.posController.getSoci(nom, cognom, tel);
        this.posController.setPtsSoci(s, pts);
    }

    @Quan("^vull consultar quants diners corresponen a aquests (\\d+) punts$")
    public void consult_pts(int pts)
    {
        this.pts = pts;
        this.money = this.posController.getMoney(pts);
    }

    @Aleshores("^la pantalla de tpv mostra$")
    public void la_pantalla_del_consult_pts(String msg) throws Throwable {
       assertEquals(msg, this.posController.getPantallaPtsSoci(this.pts, this.money));
    }

    @Donat("^vull descanviar punts de soci amb nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+), telèfon (\\d+) i (\\d+) punts per un descompte$")
    public void descanviar_pts_per_descompte(String nom, String cognom, int edat, int tel, int pts)
    {
        this.money = this.posController.getMoney(pts);
        ValDescompte valDesc = new ValDescompte(this.money, "Val descompte 5€");
        Soci s = this.posController.getSoci(nom, cognom, tel);
        this.posController.assignDescompte(s, valDesc);
    }

    @Aleshores("^el soci amb nom \"([^\"]*)\", cognom \"([^\"]*)\", edat (\\d+), telèfon (\\d+) i (\\d+) punts té el descompte \"([^\"]*)\"$")
    public void check_Descompte_Soci(String nom, String cognom, int edat, int tel, int pts, String nameDesc) throws Throwable
    {
        Soci s = this.posController.getSoci(nom, cognom, tel);
        assertTrue(this.posController.existsDescompteSoci(s, nameDesc));
    }

    //Reservas
    @I("^un producte amb nom \"([^\"]*)\", preu (\\d+)€, iva (\\d+)% i codi de barres (\\d+) que encara no esta a la venda$")
    public void createProductNotSale(String productName, int price, int vatPct, int barCode) throws Throwable {
        this.productsService.newProduct(productName, price, vatPct, barCode, false);
    }

    @Quan("^introdueix una reserva del client amb dni (\\d+) per el producte amb nom \"([^\"]*)\" amb codi de barres (\\d+) en quantitat (\\d+)$")
    public void introduirReserva(int dni, String nomProd, int codi, int quantitat) throws Throwable {
        tryCatch(() -> this.posController.introduirReserva(dni, nomProd, codi, quantitat));
    }

    @Aleshores("^hi ha una reserva al sistema pel producte amb nom \"([^\"]*)\" i el dni (\\d+)$")
    public void checkReserva(String nomProd, int dni) throws Throwable {
        assertNotNull(this.posController.getReservaByProdDni(nomProd, dni));
    }

    @I("^una reserva al sistema pel producte \"([^\"]*)\", dni (\\d+) i codi de barres (\\d+) en quantitat (\\d+)$")
    public void reservaExistent(String nom, int dni, int codi, int quantitat) throws Throwable {
        this.posController.introduirReserva(dni, nom, codi, quantitat);
    }

    @Quan("^visualitzo el llistat de totes les reserves del sistema$")
    public void showReserves() throws Throwable {
        this.posController.getMissatgeInfoTotesReserves();
    }

    @Quan("^visualitzo el llistat de les reserves del producte amb id (\\d+) del sistema$")
    public void showReservesConcret(int id) throws Throwable {
        this.posController.getMissatgeInfoReservesConcret(id);
    }

    @Aleshores("^obtinc una llista que diu: \"([^\"]*)\"$")
    public void checkLlistaTotal(String llista) throws Throwable {
        assertEquals(llista, this.posController.getMissatgeInfoTotesReserves());
    }

    @Aleshores("^per al producte amb id (\\d+) obtinc una llista que diu: \"([^\"]*)\"$")
    public void checkLlistaConcret(int id, String llista) throws Throwable {
        assertEquals(llista, this.posController.getMissatgeInfoReservesConcret(id));
    }
}



