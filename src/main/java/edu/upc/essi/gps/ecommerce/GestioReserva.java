package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xavi on 17/12/15.
 */
public class GestioReserva {
    private List<Reserva> reservaList;

    public GestioReserva() {
        reservaList = new ArrayList<>();
    }

    public void introduirReserva(long prodId, int dniComprador, String prodName, int quantitat){
        Reserva r = new Reserva(prodId, dniComprador, prodName, quantitat);
        reservaList.add(r);
    }

    //Retorna una llita amb les reserves per un producte
    public List<Reserva> comprovarReservesPerUnProducte(long prodId){
        List<Reserva> aux = new ArrayList<>();
        for(Reserva r: reservaList){
            if(r.getProdId() == prodId) aux.add(r);
        }
        return aux;
    }

    public Reserva getReservaByNomDni(String nomProd, int dni){
        for(Reserva r: reservaList){
            if (r.getDniComprador() == dni && r.getProdName().equals(nomProd)) return r;
        }
        return null;
    }

    public String getTotesReserves() {
        String s = "";
        List<Reserva> aux = reservaList;
        for (Reserva r : reservaList) {
            s += r.getProdId() + ", " + r.getDniComprador() + ", " + r.getQuantitat() + ";";
        }
        return s;
    }

    public String getReservesConcret(int id) {
        String s = "";
        List<Reserva> aux = reservaList;
        for (Reserva r : reservaList) {
            if (r.getProdId()== id) s += r.getDniComprador() + ", " + r.getQuantitat() + ";";

        }
        return s;
    }
}
