package edu.upc.essi.gps.ecommerce;


public class PercDescompte extends Descompte
{
    private int perc;

    public PercDescompte(int p, String name)
    {
        super(name);
        this.perc = p;
    }

    public int getPerc()
    {
        return this.perc;
    }

    public void setPerc(int p)
    {
        this.perc = p;
    }


    @Override
    public String getTipusDescompte(){ return "Descompte en percentatge"; }


}
