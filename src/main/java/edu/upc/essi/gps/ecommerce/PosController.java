package edu.upc.essi.gps.ecommerce;


import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import sun.security.krb5.internal.crypto.Des;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;


import static edu.upc.essi.gps.utils.Validations.*;

public class PosController {

    private final ProductsService productsService;
    private final DescomptesService descomptesService;

    private final String shop;
    private final int posNumber;
    private String currentSaleAssistantName;
    private Sale currentSale;
    private int cashTVP;
    private GestioSocis socis;
    private String dataObertura;
    private Soci sociRetornaProducte;
    private List <Product> productesARetornar;
    private GestioValDevolucio valsDeDevolucio;
    private GestioDevolucio devolucions;
    private GestioQuadrament quadraments;
    private GestioUsuaris usuaris;
    private Date currentDate;
    private int currentTpv;
    private List <Product> productesCercatsPerNom;
    private GestioSale ventesEnregistrades;
    private String missatgePagamentTargeta;
    private int actualCardCode;
    private Sale copyCurrentSale;
    private Sale auxSale;
    private PtsFedelitzacioService pfs;

    private GestioReserva gestioReserva;

    public PosController(String shop, int posNumber, ProductsService productsService, DescomptesService descomptesService) {
        this.shop = shop;
        this.posNumber = posNumber;
        this.productsService = productsService;
        this.descomptesService = descomptesService;
        this.descomptesService.clearListValsDescompte();
        this.cashTVP = 0;
        this.quadraments = new GestioQuadrament();
        this.valsDeDevolucio = new GestioValDevolucio();
        this.devolucions = new GestioDevolucio();
        //usuaris.put("admin",new Usuari(Usuari.Tipus.ADMIN, "admin", "1234"));
        this.usuaris = new GestioUsuaris();
        usuaris.afegirUsuari("admin",new Usuari(Usuari.Tipus.ADMIN, "admin", "1234"));
        this.socis = new GestioSocis();
        this.valsDeDevolucio = new GestioValDevolucio();
        this.ventesEnregistrades = new GestioSale();
        currentTpv = posNumber;
        missatgePagamentTargeta = "";
        actualCardCode = -1;
        this.copyCurrentSale = null;
        this.auxSale = null;
        this.pfs = new PtsFedelitzacioService();
        this.gestioReserva = new GestioReserva();
    }

    public void setData (String data) {
        this.dataObertura = data;
    }

    public void newUser(Usuari.Tipus tipus, String id, String pass) {
        if (currentSaleAssistantName == null) throw new IllegalStateException("newUser: Cap usuari ha fet login!");
        Usuari.Tipus tipusCurrentUser = usuaris.getUsuari(currentSaleAssistantName).getTipus();

        if (tipus == null) throw new IllegalStateException("newUser: Falta el tipus d'usuari!");
        if (id == null) throw new IllegalStateException("newUser: Falta el nom d'usuari!");
        if (pass == null) throw new IllegalStateException("newUser: Falta el password!");

        if (tipus == Usuari.Tipus.ADMIN && tipusCurrentUser != Usuari.Tipus.ADMIN)
            throw new IllegalStateException("newUser: Permis denegat a "+currentSaleAssistantName);

        if (tipus == Usuari.Tipus.GESTOR &&
                (tipusCurrentUser != Usuari.Tipus.ADMIN && tipusCurrentUser != Usuari.Tipus.GESTOR))
            throw new IllegalStateException("newUser: Permis denegat a "+currentSaleAssistantName);

        if (tipus == Usuari.Tipus.VENEDOR &&
                (tipusCurrentUser != Usuari.Tipus.ADMIN && tipusCurrentUser != Usuari.Tipus.GESTOR))
            throw new IllegalStateException("newUser: Permis denegat a "+currentSaleAssistantName);

        if (this.usuaris.containsKey(id))
            throw new IllegalStateException("Aquest usuari ja existeix");

        usuaris.afegirUsuari(id, new Usuari(tipus, id, pass));
    }

    public void login(String saleAssistantName) {
        checkNotNull(saleAssistantName, "saleAssistantName");
        if (this.currentSaleAssistantName != null)
            throw new IllegalStateException("Aquest tpv està en ús per " + this.currentSaleAssistantName);
        this.currentSaleAssistantName = saleAssistantName;
    }

    public void login(String saleAssistantName, String saleAssistantPassword) {
        checkNotNull(saleAssistantName, "saleAssistantName");
        checkNotNull(saleAssistantPassword, "saleAssistantPassword");
        if (currentSaleAssistantName != null) {
            throw new IllegalStateException("Aquest tpv està en ús per "+currentSaleAssistantName);
        }
        if (this.usuaris != null) {
            if (!this.usuaris.containsKey(saleAssistantName)) {
                throw new IllegalStateException("Aquest usuari no està registrat");
            }
            if (!this.usuaris.getUsuari(saleAssistantName).getPassword().equals(saleAssistantPassword)) {
                throw new IllegalStateException("Contrasenya incorrecta");
            }
        }
        this.currentSaleAssistantName = saleAssistantName;
    }

    public void logout() {
        if (currentSaleAssistantName == null) throw new IllegalStateException("Cap usuari ha fet login!");
        currentSaleAssistantName = null;
    }

    public void doSale (String venedor, String preu, String data, String hora, String botiga) {
        int pos = GestioSale.getSales().size();
        Sale compra = new Sale(botiga, preu, venedor, data, hora, pos);
        GestioSale.afegirSale(compra);
    }

    public String obteHistorialVentes() {
        List<Sale> llista = GestioSale.getSales();
        String v, d, h, b, p;
        String resultat = "Venedor     | Data           | Hora       | Botiga       | Preu";
        for (int i = 0; i < llista.size(); i++) {
            v = llista.get(i).getSaleAssistantName();
            d = llista.get(i).getDate();
            h = llista.get(i).getTime();
            b = llista.get(i).getShop();
            p = llista.get(i).getTotal()+"€";
            resultat+="\n"+v+"          "+d+"       "+h+"     "+b+"         "+p;
        }
        return resultat;
    }

    public void startSale() {}

    public void startSale(String data, String hora) {
        if (this.currentSale != null) throw new IllegalStateException("Aquest tpv ja té una venta iniciada");
        if (this.currentSaleAssistantName == null) throw new IllegalStateException("No es pot iniciar una venta sense haver iniciat el torn!");
        this.currentSale = new Sale(shop, posNumber, currentSaleAssistantName, data, hora, descomptesService);
    }

    public String getCurrentSaleAssistantName() {
        return currentSaleAssistantName;
    }

    public Sale getCurrentSale() {
        return currentSale;
    }

    public void addProductByBarCode(int barCode) {
        if (currentSale == null) throw new IllegalStateException("No hi ha cap venta iniciada");
        Product p = productsService.findByBarCode(barCode);
        currentSale.addProduct(p, 1);
        quadraments.plasmarIngres(currentSaleAssistantName, currentTpv, currentDate, p.getPriceWithIva());
    }

    public void addProductByName(String name) {
        if (currentSale == null) throw new IllegalStateException("No hi ha cap venta iniciada");
        Product p = productsService.findByName(name);
        currentSale.addProduct(p, 1);
    }

    public String getCustomerScreenMessage() {
        String welcomeMessage = "Li donem la benvinguda a Joguets i Joguines!";
        if (currentSale == null) return welcomeMessage;
        if (currentSale.isEmpty()) {
            return welcomeMessage + "\n"+"L'atén " + currentSale.getSaleAssistantName()
                    + "\n" + currentSale.getDate() + " " + currentSale.getTime();
        }
        StringBuilder sb = new StringBuilder();
        for (SaleLine sl : currentSale.getLines()) {
            sb.append(sl.getProductName()).append(" - ")
                    .append(sl.getUnitPrice()).append("€/u x ").append(sl.getAmount()).append("u = ")
                    .append(sl.getAmount()*sl.getUnitPrice()).append("€\n");
            if (sl.getPreuTotal() < sl.getAmount()*sl.getUnitPrice()) {
                sb.append("    Descompte: "+sl.getDiscountName()+ " = -"+(sl.getAmount()*sl.getUnitPrice()-sl.getPreuTotal())+"€\n");
            }
        }
        if (currentSale.getDescomptatVals() > 0) {
            sb.append("---\n"+currentSale.getValDescompteVenda().getName()+ " = -"+currentSale.getDescomptatVals()+"€\n");
        }
        sb.append("---\n").append("Total: ").append(currentSale.getPreuTotal()).append("€");
        return sb.toString();
    }

    public int cashPayment(int delivered) {
        if (currentSale == null) throw new IllegalStateException("No es pot cobrar una venta si no està iniciada");
        if (currentSale.getLines().isEmpty())
            throw new IllegalStateException("No es pot cobrar una venta sense cap producte");
        if (currentSale.getTotal() > delivered)
            throw new IllegalStateException("El client no ha entregat suficients diners en metàlic");
        currentSale.setChargeType("Metalic");
        currentSale.setEntregat(delivered);
        int total = currentSale.getTotal();
        currentSale.setChange(delivered - total);
        ventesEnregistrades.afegirSale(currentSale);
        this.copyCurrentSale = currentSale;
        currentSale = null;
        int result = delivered - total;
        return result;
    }

    public int cardPayment(int delivered)
    {
        if (currentSale == null) throw new IllegalStateException("No es pot cobrar una venta si no està iniciada");
        if (currentSale.getLines().isEmpty())
            throw new IllegalStateException("No es pot cobrar una venta sense cap producte");
        currentSale.setChargeType("Targeta");
        currentSale.setEntregat(delivered);
        currentSale.setChange(0);
        GestioSale.afegirSale(currentSale);
        this.copyCurrentSale = currentSale;
        currentSale = null;
        return 0;
    }

    // Inicio fer descomptes en certs pcts
    public String getPantallaFerDescs()
    {
        StringBuilder missatge = new StringBuilder();

        List<Product> listProducts = productsService.listProductsByName();
        missatge.append("Llista de tots els productes:" + System.getProperty("line.separator"));
        for(Product p : listProducts)
        {
            missatge.append(p.getName() + System.getProperty("line.separator"));
        }
        missatge.append("----" + System.getProperty("line.separator"));
        missatge.append("Llista de productes comprats:");
        missatge.append(System.getProperty("line.separator"));
        List<SaleLine> lines = currentSale.getLines();
        for(int i = 0; i < lines.size(); ++i)
        {
            SaleLine sl = lines.get(i);
            missatge.append((i+1) + ": " + sl.getProductName()+" ------ ");
            missatge.append(sl.getUnitPrice() + " * ");
            missatge.append(sl.getAmount() + " unit(s) = ");
            missatge.append(sl.getTotalPrice()+"€");
            missatge.append(System.getProperty("line.separator"));
        }
        missatge.append("----" + System.getProperty("line.separator"));
        missatge.append("Descomptes aplicats:" + System.getProperty("line.separator"));

        LinkedHashMap<String, LinkedHashMap<String, Integer>> discounts = currentSale.getInfoDescPcts();
        LinkedHashMap <String, ArrayList<Product>> desc = currentSale.getDescuentos();
        int acc = 0;
        if(desc != null)
        {
            Iterator iter1 = discounts.keySet().iterator();
            while(iter1.hasNext())
            {
                String descName = (String) iter1.next();
                LinkedHashMap<String, Integer> infoDesc = new LinkedHashMap<String, Integer>(discounts.get(descName));
                Descompte discount = descomptesService.findByName(descName);
                String type = discount.getTipusDescompte();
                Iterator iter2 = infoDesc.keySet().iterator();
                if(type.equals("Descompte en percentatge"))
                {
                    int perc = discount.getPerc();
                    missatge.append(descName + ":");
                    while (iter2.hasNext()) {
                        missatge.append(System.getProperty("line.separator"));
                        String prodName= (String) iter2.next();
                        int cant = infoDesc.get(prodName);
                        Product p = productsService.findByName(prodName);
                        double d = p.getPrice()*cant;
                        double aux = d;
                        d = d * (double) (100-perc)/(double) 100;
                        missatge.append(prodName + " x " + cant + "u = -" + (int) ((int)(aux) - (int)(d))+ "€");
                        acc += ((int)(aux) - (int)(d));
                    }
                }
                else if(type.equals("Descompte X per Y"))
                {
                    missatge.append(descName + ":");
                    int x = discount.getX();
                    int y = discount.getY();
                    while (iter2.hasNext())
                    {
                        missatge.append(System.getProperty("line.separator"));
                        String prodName= (String) iter2.next();
                        Product p = productsService.findByName(prodName);
                        int cant = infoDesc.get(prodName);
                        if(cant == x)
                        {
                            missatge.append(prodName + " x " + cant + "u = -" + (int)(p.getPrice()*(x - y)) + "€");
                            acc += (p.getPrice()*(x - y));
                        }
                    }
                }
                else if(type.equals("Descompte de val"))
                {
                    missatge.append(descName + ":");
                    int val = discount.getVal();
                    while (iter2.hasNext())
                    {
                        missatge.append(System.getProperty("line.separator"));
                        String prodName = (String) iter2.next();
                        Product p = productsService.findByName(prodName);
                        int cant = infoDesc.get(prodName);
                        missatge.append(prodName + " x " + cant + "u = -" + (int)val + "€");
                        acc += val;
                    }
                }
                missatge.append(System.getProperty("line.separator"));
            }
        }

        missatge.append("*Total aplicant descomptes*: " + (currentSale.getTotal()-acc) +"€");
        String tiquet = missatge.toString();
        currentSale = null;
        return tiquet;

    }
    // Fin fer descomptes en certs pcts

    // INICIO imprimir tiquet


    public void setEntregatTargeta(int delivery)
    {
        currentSale.setChargeType("Targeta");
        currentSale.setEntregat(delivery);
        currentSale.setChange(0);
    }

    public String imprimirTicket()
    {
        StringBuilder missatge = new StringBuilder();
        //missatge.append("Article  Preu     Unitat      Preu Total");
        //missatge.append(System.getProperty("line.separator"));

        auxSale = this.copyCurrentSale;
        List<SaleLine> lines = auxSale.getLinesForPrint();
        for(int i = 0; i < lines.size(); ++i)
        {
            SaleLine sl = lines.get(i);
            missatge.append((i+1) + ": " + sl.getProductName()+" ------ ");
            missatge.append(sl.getUnitPrice() + " * ");
            missatge.append(sl.getAmount() + " unit(s) = ");
            missatge.append(sl.getTotalPrice()+"€");
            missatge.append(System.getProperty("line.separator"));
        }

        String pagament = auxSale.getChargeType();
        String data = auxSale.getDate();
        String hora = auxSale.getTime();
        String shop = auxSale.getShop();
        int posNumber = auxSale.getPosNumber();
        String assistant = auxSale.getSaleAssistantName();

        LinkedHashMap<Integer, Integer> ivas = auxSale.getIvas();

        missatge.append("----" + System.getProperty("line.separator") +
                "I.V.A:" + System.getProperty("line.separator"));

        Iterator iter = ivas.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            Integer iva = (Integer) entry.getKey();
            Integer preu = (Integer) entry.getValue();
            double i = (double) iva.intValue();
            double pre = (double) preu.intValue();
            missatge.append((int) (pre/((100+i)/100)) + "€ + " + entry.getKey() + "%: " + entry.getValue() + "€");
            missatge.append(System.getProperty("line.separator"));
        }
        missatge.append("----" + System.getProperty("line.separator"));
        missatge.append("Total sense descompte: " + auxSale.getTotalSenseDescompte()+"€");
        missatge.append(System.getProperty("line.separator"));
        if (auxSale.hihaValDevolucio()) {
            missatge.append("Val de devolucio: " + auxSale.obteDescompteDevolucio()+"€");
            missatge.append(System.getProperty("line.separator"));
        }
        LinkedHashMap<String, LinkedHashMap<String, Integer>> discounts = auxSale.getInfoDescPcts();
        LinkedHashMap <String, ArrayList<Product>> desc = auxSale.getDescuentos();
        int acc = 0;
        if(desc != null)
        {
            Iterator iter1 = discounts.keySet().iterator();
            while(iter1.hasNext())
            {
                String descName = (String) iter1.next();
                LinkedHashMap<String, Integer> infoDesc = new LinkedHashMap<String, Integer>(discounts.get(descName));
                Descompte discount = descomptesService.findByName(descName);
                String type = discount.getTipusDescompte();
                Iterator iter2 = infoDesc.keySet().iterator();
                if(type.equals("Descompte en percentatge"))
                {
                    int perc = discount.getPerc();
                    missatge.append(descName + ":");
                    while (iter2.hasNext()) {
                        missatge.append(System.getProperty("line.separator"));
                        String prodName= (String) iter2.next();
                        int cant = infoDesc.get(prodName);
                        Product p = productsService.findByName(prodName);
                        double d = p.getPrice()*cant;
                        double aux = d;
                        d = d * (double) (100-perc)/(double) 100;
                        missatge.append(prodName + " x " + cant + "u = -" + (int) ((int)(aux) - (int)(d))+ "€");
                        acc += ((int)(aux) - (int)(d));
                    }
                }
                else if(type.equals("Descompte X per Y"))
                {
                    missatge.append(descName + ":");
                    int x = discount.getX();
                    int y = discount.getY();
                    while (iter2.hasNext())
                    {
                        missatge.append(System.getProperty("line.separator"));
                        String prodName= (String) iter2.next();
                        Product p = productsService.findByName(prodName);
                        int cant = infoDesc.get(prodName);
                        if(cant == x)
                        {
                            missatge.append(prodName + " x " + cant + "u = -" + (int)(p.getPrice()*(x - y)) + "€");
                            acc += (p.getPrice()*(x - y));
                        }
                    }
                }
                else if(type.equals("Descompte de val"))
                {
                    missatge.append(descName + ":");
                    int val = discount.getVal();
                    while (iter2.hasNext())
                    {
                        missatge.append(System.getProperty("line.separator"));
                        String prodName = (String) iter2.next();
                        Product p = productsService.findByName(prodName);
                        int cant = infoDesc.get(prodName);
                        missatge.append(prodName + " x " + cant + "u = -" + (int)val + "€");
                        acc += val;
                    }


                }
                missatge.append(System.getProperty("line.separator"));
            }
            //missatge.append("----" + System.getProperty("line.separator"));
        }

        missatge.append("Total amb descompte: " + (auxSale.getTotal()-acc) +"€");
        missatge.append(System.getProperty("line.separator"));
        missatge.append("Entregat: " + auxSale.getEntregat()+"€");
        missatge.append(System.getProperty("line.separator"));
        missatge.append("Canvi: " + (auxSale.getChange()+acc) + "€");
        missatge.append(System.getProperty("line.separator"));
        missatge.append("Tipus de pagament: " + pagament);
        missatge.append(System.getProperty("line.separator"));
        missatge.append("Gestor: " + assistant);
        missatge.append(System.getProperty("line.separator"));
        missatge.append("Num de TPV: " + posNumber);
        missatge.append(System.getProperty("line.separator"));
        missatge.append("Botiga: " + shop);
        missatge.append(System.getProperty("line.separator"));

        missatge.append("Data: " + hora + " " + data);

        String tiquet = missatge.toString();
        auxSale = null;
        return tiquet;
    }
    // FIN imprimir tiquet

    //INICI consultar socis

    public void iniciarGestioSocis () {
        this.socis = new GestioSocis();
    }
    public void crearSoci (String nom, String cognom, int edat, int telefon) {
        if (this.socis == null) throw new IllegalStateException("No hi ha gestio de socis iniciada");
        boolean existent = false;
        try {
            Soci s = this.socis.getSoci(nom, cognom, telefon);
            existent = true;
        }
        catch (Exception e) {
            this.socis.afegir(nom, cognom, edat, telefon);
            Soci s = this.socis.getSoci(nom, cognom, telefon);
            this.pfs.addSoci(s);
        }
        if (existent) throw new IllegalArgumentException("Aquest soci ja existeix");
    }

    public void afegirCompra (String nom, String cognom, int telefon, String data, String hora) {
        Sale compra = new Sale(this.shop, this.posNumber, this.currentSaleAssistantName, data, hora, descomptesService);
        this.socis.getSoci(nom, cognom, telefon).afegirCompra(compra);
    }
    public List<Soci> getTotsSocis () {
        if (this.socis == null) throw new IllegalStateException("No hi ha gestio de socis iniciada");
        return this.socis.getTots();
    }

    public int getNSocis () {
        if (this.socis == null) throw new IllegalStateException("No hi ha gestio de socis iniciada");
        return this.socis.getNSocis();
    }

    public boolean existSociAmb (String nom, String cognom, int edat, int telefon, int punts) {
        if (this.socis == null) throw new IllegalStateException("No hi ha gestio de socis iniciada");
        int i = 0;
        boolean trobat = false;
        List <Soci> socis = this.socis.getTots();
        while (i < socis.size() && !trobat) {
            Soci s = socis.get(i);
            if (nom.equals(s.getNom()) && cognom.equals(s.getCognom()) && edat == s.getEdat() && telefon == s.getTel() && punts == s.getPunts()) {
                trobat = true;
            }
            i++;
        }
        return trobat;
    }
    public String getMissatgeSocis () {
        if (this.socis == null) throw new IllegalStateException("No hi ha gestio de socis iniciada");
        StringBuilder missatge = new StringBuilder();
        missatge.append("NOM COGNOM EDAT TELEFON PUNTS\n---");
        for (Soci soci : this.socis.getTots()) {
            missatge.append("\n").append(soci.getNom()).append(" ").append(soci.getCognom()).append(" ")
                    .append(soci.getEdat()).append(" ").append(soci.getTel()).append(" ").append(soci.getPunts());
        }
        return missatge.toString();
    }

    public Soci getSoci (String nom, String cognom, int telefon) {
        if (this.socis == null) throw new IllegalStateException("No hi ha gestio de socis iniciada");
        return this.socis.getSoci(nom, cognom, telefon);
    }

    public String getMissatgeDetallsSoci (Soci soci, String txt) {
        StringBuilder missatge = new StringBuilder();
        String e = " ";
        missatge.append("NOM COGNOM EDAT TELEFON PUNTS\n---\n").append(soci.getNom()).append(e)
                .append(soci.getCognom()).append(e).append(soci.getEdat()).append(e).append(soci.getTel())
                .append(e).append(soci.getPunts())
                .append("\n---\nHa fet les compres:\nDATA HORA BOTIGA TPV VENEDOR");
        for (Sale compra : soci.getCompres()) {
            missatge.append("\n").append(compra.getDate()).append(e).append(compra.getTime()).append(e)
                    .append(compra.getShop()).append(e).append(compra.getPosNumber()).append(e).append(compra.getSaleAssistantName());
        }
        return missatge.toString();
    }
    //FIN consultar socis


    //INICIO zona crear val devolucio
    public void iniciarGestioValsDevolucio () {
        this.valsDeDevolucio = new GestioValDevolucio();
    }
    public void iniciarDevolucioAmbSoci (String nom, String cognom, int telefon) {
        this.sociRetornaProducte = this.socis.getSoci(nom, cognom, telefon);
        this.productesARetornar = new ArrayList<>();
    }

    public void iniciarDevolucioNoSoci () {
        this.sociRetornaProducte = null;
        this.productesARetornar = new ArrayList<>();
    }

    public void afegirProducteDevolucio (String nomProducte, int codiProducte) {
        if (codiProducte == 0) this.productesARetornar.add(productsService.findByName(nomProducte));
        else this.productesARetornar.add(productsService.findByBarCode(codiProducte));
    }

    public void afegirValDevolucio (String temps, String motiu) {
        if (!this.productesARetornar.isEmpty()) this.valsDeDevolucio.afegir(this.dataObertura, temps, motiu, this.sociRetornaProducte, this.productesARetornar, this.posNumber, this.currentSaleAssistantName);
        else throw new IllegalStateException("No hi ha productes retornades");
    }

    public ValDevolucio getValDevolucio (String data, String hora, int posNumber) {
        return this.valsDeDevolucio.getValDevolucio(data, hora, posNumber);
    }

    public void usarValDevolucio (String data, String hora, int posNumber) {
        ValDevolucio val = valsDeDevolucio.getValDevolucio(data, hora, posNumber);
        if (!val.valUsat()) {
            this.currentSale.aplicarValDevolucio(val.getQuantitat());
            val.validarValDevolucio();
        }
        else {
            throw new IllegalStateException("Val de devolucio usat");
        }
    }


    //FIN zona crear val devolucio

    //INICI zona afegir devolucio
    public void afegirDevolucio (String temps, String motiu) {
        if (!this.productesARetornar.isEmpty()) {
            ValDevolucio valDevolucio = this.valsDeDevolucio.getValDevolucio(this.dataObertura, temps, this.posNumber);
            this.devolucions.afegirDevolucio(this.sociRetornaProducte, motiu, this.productesARetornar, this.dataObertura, temps, this.posNumber, this.shop, this.currentSaleAssistantName, valDevolucio);
            this.quadraments.plasmarIngres(currentSaleAssistantName, currentTpv, currentDate, -this.devolucions.obteDevolucio(this.dataObertura, temps, this.posNumber).obteQuantitatDiners());
        }
        else throw new IllegalStateException("No hi ha productes retornades");
    }

    //soci nom, soci cognom, quantitat, motiu, datacreacio, horacreacio, posnumber, botiga, venedor, val devolucio
    //productes retornats
    public String consultarDevolucions () {
        StringBuilder msg = new StringBuilder("NOM I COGNOM DEL SOCI | QUANTITAT(€) DE DINERS | MOTIU DEVOLUCIO | DATA I HORA DEVOLUCIO | NUMERO TPV | BOTIGA | VENEDOR | HA DEMANAT VAL DEVOLUCIO");
        msg.append(System.getProperty("line.separator"));
        for (Devolucio dev : this.devolucions.obteDevolucions()) {
            msg.append(dev.obteInfo()).append(System.getProperty("line.separator"));
        }
        return msg.toString();
    }


    //FI zona afegir devolucio

    //Operacions amb quadraments

    public void afegirQuadrament(Quadrament q){
        quadraments.afegirQuadrament(q);
    }

    public String visualitzarQuadraments(){
        return quadraments.visualitzarQuadraments();
    }

    public void creaQuadrament(float recompteInicial) {
        quadraments.creaQuadrament(currentSaleAssistantName, currentTpv, currentDate, recompteInicial);
    }

    public void introduirEfectiuFinalDia(float imReal){
        quadraments.introduirEfectiuFinalDia(currentSaleAssistantName, currentTpv, currentDate, imReal);
    }

    public void tancarQuadramentIncorrecte(){
        quadraments.tancarQuadramentIncorrecte(currentSaleAssistantName, currentTpv, currentDate);
    }

    public String getMsgQuadrament(){
        return quadraments.getMsgQuadrament(currentSaleAssistantName, currentTpv, currentDate);
    }

    public Quadrament getMostRecentQuadrament(){
        return quadraments.getMostRecentQuadrament();
    }

    public void setCurrentDate(Date date){
        currentDate = date;
    }

    public void setCurrentTpv(int tpv){
        currentTpv = tpv;
    }

    public boolean mateixQuadramentActual (int quadrament) {
        return this.quadraments.getQuadrament(currentSaleAssistantName, currentTpv, currentDate).mateixQuadrament(quadrament);
    }

    // INICI Fer descomptes

    public void addProductByBarCodeWithUnit(int codeBar, int unit)
    {
        if (currentSale == null) throw new IllegalStateException("No hi ha cap venta iniciada");
        Product p = productsService.findByBarCode(codeBar);
        currentSale.addProduct(p, unit);
        quadraments.plasmarIngres(currentSaleAssistantName, currentTpv, currentDate, p.getPriceWithIva());
    }

    public void newPctExisteix(String name, int price, int vatPct, int codeBar)
    {
        if (currentSaleAssistantName  == null) throw new IllegalStateException("No hi ha cap torn iniciat");

        this.productsService.newProduct(name, price, vatPct, codeBar);
    }

    public void newPercDescompte(String name, int perc)
    {
        if (currentSaleAssistantName  == null) throw new IllegalStateException("No hi ha cap torn iniciat");

        this.descomptesService.createPercDescompte(perc, name);
    }

    public void newXYDescompte(String name, int x, int y)
    {
        if (currentSaleAssistantName  == null) throw new IllegalStateException("No hi ha cap torn iniciat");
        this.descomptesService.createXYDescompte(x, y, name);
    }

    public void newValDescompte(String name, int val)
    {
        if (currentSaleAssistantName  == null) throw new IllegalStateException("No hi ha cap torn iniciat");
        this.descomptesService.createValDescompte(val, name);
    }

    public void assignPerProduct(String pctName, String nomDescomp)
    {
        if (currentSaleAssistantName  == null) throw new IllegalStateException("No hi ha cap torn iniciat");

        this.descomptesService.insertDescPct(pctName, nomDescomp);
        String type = descomptesService.findByName(nomDescomp).getTipusDescompte();
        this.productsService.findByName(pctName).setDescomptePct(type, nomDescomp);
    }

    public Descompte findDescompteByName(String name)
    {
        if (currentSaleAssistantName  == null) throw new IllegalStateException("No hi ha cap torn iniciat");

        return this.descomptesService.findByName(name);
    }

    public ProductsService getProductsService()
    {
        return this.productsService;
    }

    /*public String listPctInfo(Descompte discount)
    {
        String descName = discount.getName();
        LinkedHashMap <String, LinkedHashMap <String, Integer>> infoDescPcts = currentSale.getInfoDescPcts();
        LinkedHashMap<String, Integer> infoDescPct = new LinkedHashMap<String, Integer>(infoDescPcts.get(descName));
        return this.descomptesService.getPctsInfo(discount, infoDescPct, productsService);
    }*/

    //FIN Fer descomptes

    //Inici Afegir producte a la venda

    public void searchProductsByName(String name) {
        productesCercatsPerNom = new ArrayList<>();
        List <Product> lP = productsService.listProductsByName();
        for (Product p : lP) {
            if (p.getName().contains(name)) productesCercatsPerNom.add(p);
        }
    }

    public List <Product> getProductesCercatsPerNom() {
        return productesCercatsPerNom;
    }

    public String getSaleAssistantScreenMessage() {
        if (productesCercatsPerNom.isEmpty()) return "No s'ha trobat cap producte amb aquest nom";
        else {
            StringBuilder ret = new StringBuilder();
            if (productesCercatsPerNom.size() == 1)  ret.append("S'ha trobat 1 producte amb aquest nom:");
            else ret.append("S'han trobat "+productesCercatsPerNom.size()+" productes amb aquest nom:\n");
            ret.append("---");
            int c = 1;
            for (Product p : productesCercatsPerNom) {
                ret.append("\n"+c+": "+p.getName()+" - Preu: "+p.getPrice()+"€ - Codi de barres: "+p.getBarCode());
                ++c;
            }
            return ret.toString();
        }
    }
    //Final afegir producte a la venda

    //INICI afegir val de descompte a la venda
    public void crearValDescompteTot(int codi, String name, String tipus, String caducitat, int cmin, int var) {
        if (existeixValDescompte(codi)) throw new IllegalStateException("Val ja existent al sistema");
        descomptesService.crearValDescompteTot(codi, name, tipus, caducitat, cmin, var);
    }

    public void crearValDescompteXxY(int codi, String name, String tipus, String caducitat, String prod, int uTot, int uPag) {
        try {
            Product p = productsService.findByName(name);
        }
        catch (Exception e){
            throw new IllegalArgumentException("Aquest val de descompte s'aplica sobre un producte no existent al sistema");
        }
        if (existeixValDescompte(codi)) throw new IllegalStateException("Val ja existent al sistema");
        descomptesService.crearValDescompteXxY(codi, name, tipus, caducitat, prod, uTot, uPag);
    }

    public boolean existeixValDescompte(int codi) {
        return descomptesService.existeixVal(codi);
    }

    public void afegirValDescompte(int codi) {
        ValDescompte vd = findValDescompte(codi);
        if (vd.isUsat()) throw new IllegalStateException("El codi introduït correspon a un val de descompte ja utilitzat");
        Sale s = getCurrentSale();
        DateFormat formatter;
        Date dataVenda = new Date();
        Date dataCaducitat = new Date();
        formatter = new SimpleDateFormat("dd-mm-yyyy");
        try {
            dataVenda = formatter.parse(s.getDate());
            dataCaducitat = formatter.parse(vd.getCaducitat());
        } catch (ParseException e) {
        }
        if (dataCaducitat.before(dataVenda)) throw new IllegalStateException("El codi introduït correspon a un val de descompte caducat");
        if (vd.getTipusVal().equals("XxY")) {
            for (SaleLine sl : s.getLines()) {
                if (sl.getProductName().equals(vd.getProducte())) {
                    if (sl.getDescXYAplicat() > 0) throw new IllegalStateException("El producte del val ja té un descompte de XxY associat");
                    if (vd.getUnitatsTotals() > sl.getAmount()) throw new IllegalStateException("No hi ha prou unitats del producte del val de descompte");
                }
            }
        }
        else if (vd.getCompraMinima() > s.getPreuTotal()) throw new IllegalStateException("El total de la venda actual és inferior al mínim requerit pel val de descompte");

        vd.setUsat(true);
        s.addValDescompte(vd);
    }

    public ValDescompte findValDescompte(int codi) {
       return descomptesService.findByCode(codi);
    }

    public void usarValDescompte (int codi) {
        ValDescompte vd = findValDescompte(codi);
        vd.setUsat(true);
    }
    //FI afegir val de descompte a la venda

    //INICI PAGAR AMB TARGETA
    public void cardPayMode(int cardCode, int pin) {
        if (currentSale == null) throw new IllegalStateException("No es pot cobrar una venta si no està iniciada");
        if (currentSale.getLines().isEmpty())
            throw new IllegalStateException("No es pot cobrar una venta sense cap producte");
        if (cardCode == -1) cardCode = this.actualCardCode;
        else this.actualCardCode = cardCode;
        missatgePagamentTargeta = "";
        CardPaymentService cps = new CardPaymentService();
        int total = currentSale.getPreuTotal();
        if (!cps.tePin(cardCode)) {
            try {
                cps.CardPayment(cardCode, total, -1);
                cardPayment(total);
                this.actualCardCode = -1;
                setMissatgePagamentTargeta("Operació realitzada correctament. Import pagat: "+total+"€");
            }
            catch (Exception e) {
                setMissatgePagamentTargeta("Targeta sense fons. Operació no realitzada.");
            }
        }
        else {
            if (pin == -1) setMissatgePagamentTargeta("Introdueixi el seu pin, si us plau.");
            else {
                try {
                    cps.CardPayment(cardCode, total, pin);
                    cardPayment(total);
                    this.actualCardCode = -1;
                    setMissatgePagamentTargeta("Operació realitzada correctament. Import pagat: "+total+"€");
                }
                catch (Exception e) {
                    setMissatgePagamentTargeta("Pin incorrecte. Operació no realitzada.");
                }
            }
        }
    }


    public String getMissatgePagamentTargeta() {
        return missatgePagamentTargeta;
    }

    public void setMissatgePagamentTargeta(String m) {
        missatgePagamentTargeta = m;
    }

    public int getActualCardCode() {
        return actualCardCode;
    }

    public void setActualCardCode(int actualCardCode) {
        this.actualCardCode = actualCardCode;
    }

    //FINAL PAGAR AMB TARGETA

    public boolean existVenedor(String dni, String password) {
        if (this.usuaris == null) throw new IllegalStateException("No hi ha gestio de venedors iniciada.");
        Usuari aux = new Usuari (Usuari.Tipus.VENEDOR, dni, password);
        return this.usuaris.containsUser(aux);
    }

    public String getMissatgeInfoVenedors(){
        return usuaris.getInfoVenedors();
    }

    //Inicio descanviar pts per descompte
    public String getPantallaPtsSoci(int pts, int money)
    {
        StringBuilder missatge = new StringBuilder();
        missatge.append(pts + " punts corresponen " + money + "€");
        String message = missatge.toString();
        return message;
    }

    public void setPtsSoci(Soci s, int pts)
    {
        pfs.setPtsSoci(s, pts);
    }

    public int getMoney(int pts)
    {
        return pfs.getMoneyFromPts(pts);
    }

    public void assignDescompte(Soci s, Descompte descompte)
    {
        s.addDescompte(descompte);
    }

    public Boolean existsDescompteSoci(Soci s, String nameDesc) {
        Boolean found = false;
        if (this.socis == null) throw new IllegalStateException("No hi ha gestio de socis iniciada");
        List<Descompte> listD = s.getDescomptes();
        for(Descompte d: listD)
        {
            if(d.getName().equals(nameDesc))
                return true;
        }
        return found;
    }
    public void introduirReserva(int dniComprador, String nomProd, int codiBarres, int quantitat){
        this.searchProductsByName(nomProd);
        List<Product> llista = getProductesCercatsPerNom();
        boolean exists = false;
        long prodId = -1;
        for(Product p: llista){
            if (p.getBarCode() == codiBarres){
                exists = true;
                if(!p.isProductOnSale()) prodId = p.getId();
            }
        }
        if (prodId != -1) gestioReserva.introduirReserva(prodId, dniComprador, nomProd, quantitat);
        else if (exists) throw new IllegalStateException("Producte ja disponible a la botiga");
        else throw new IllegalStateException("El producte a reservar no existeix");
    }

    public Reserva getReservaByProdDni(String nomProd, int dni){
        return gestioReserva.getReservaByNomDni(nomProd, dni);
    }

    public String getMissatgeInfoTotesReserves() {
        return gestioReserva.getTotesReserves();
    }

    public String getMissatgeInfoReservesConcret(int id) {
        return gestioReserva.getReservesConcret(id);
    }
}
