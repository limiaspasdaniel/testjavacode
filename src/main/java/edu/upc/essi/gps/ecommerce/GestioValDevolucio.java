package edu.upc.essi.gps.ecommerce;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Lei on 30/11/2015.
 */
public class GestioValDevolucio {
    private List <ValDevolucio> valsDevolucio;

    public GestioValDevolucio () {
        this.valsDevolucio = new ArrayList<>();
    };

    //soci=null si client no soci
    public void afegir (String dataDevolucio, String horaDevolucio, String motiu, Soci soci, List <Product> productesDevolucio, int posNumber, String saleAssistantName) {
        boolean trobat = false;
        int i = 0;
        while (i < this.valsDevolucio.size() && !trobat) {
            ValDevolucio val = valsDevolucio.get(i);
            if (val.getDataDevolucio().equals(dataDevolucio) && val.getHoraDevolucio().equals(horaDevolucio) && val.getPosNumber() == posNumber) {
                trobat = true;
            }
            else {
                i++;
            }
        }
        if (!trobat) {
            ValDevolucio valdevolucio = new ValDevolucio(dataDevolucio, horaDevolucio, motiu, soci, productesDevolucio, posNumber, saleAssistantName);
            this.valsDevolucio.add(valdevolucio);
        }
        else {
            throw new IllegalStateException ("Ja existeix el val de devolucio amb la data, hora i tpv donada");
        }
    }

    public ValDevolucio getValDevolucio(String dataDevolucio, String horaDevolucio, int posNumber) {
        boolean trobat = false;
        int i = 0;
        ValDevolucio valD = null;
        while (i < this.valsDevolucio.size() && !trobat) {
            ValDevolucio val = valsDevolucio.get(i);
            if (val.getDataDevolucio().equals(dataDevolucio) && val.getHoraDevolucio().equals(horaDevolucio) && val.getPosNumber() == posNumber) {
                trobat = true;
                valD = val;
            }
            else {
                i++;
            }
        }
        return valD;
    }
}
