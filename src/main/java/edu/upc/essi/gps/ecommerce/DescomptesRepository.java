package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Matchers;
import edu.upc.essi.gps.utils.Repository;

import java.util.ArrayList;


public class DescomptesRepository extends Repository<Descompte>
{

    private static ArrayList<Descompte> listDescomptes = new ArrayList<Descompte>();
    private static ArrayList<ValDescompte> listValsDescompte = new ArrayList<>();

    public Descompte findByName(final String name) {
        return find(Matchers.nameMatcher(name));
    }

    public ValDescompte findByCode(final int code) throws RuntimeException {
        for (ValDescompte vd : listValsDescompte) {
            if (vd.getCodi() == code) return vd;
        }
        throw new IllegalArgumentException("Aquest codi no correspon a cap val de descompte registrat al sistema");
    }

    public boolean existeixVal(int codi) {
        for (ValDescompte vd : listValsDescompte) {
            if (vd.getCodi() == codi) return true;
        }
        return false;
    }

    @Override
    protected void checkInsert(final Descompte entity) throws RuntimeException {
        if(findByName(entity.getName())!=null)
            throw new IllegalArgumentException("Ja existeix un descompte amb aquest nom");
    }

    public void insertVal(final ValDescompte entity) throws RuntimeException {
        if(existeixVal(entity.getCodi())) {
            throw new IllegalArgumentException("Val ja existent al sistema");
        }
        else listValsDescompte.add(entity);
    }

    public ArrayList<ValDescompte> listValsDescompte() {
        return listValsDescompte;
    }

    public void clearListValsDescompte() {
        listValsDescompte = new ArrayList<>();
    }
}
