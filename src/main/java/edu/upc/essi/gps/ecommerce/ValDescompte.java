package edu.upc.essi.gps.ecommerce;


/**
 * Created by mingjian on 02/12/2015.
 */
public class ValDescompte extends Descompte
{

    private int val;
    private int codi;
    private String caducitat;
    private boolean usat;
    private int compraMinima;
    private String tipusVal;

    //import total
    private int importDescomptat;

    //percentatge total
    private int percDesc;

    //XxY
    private String producte;
    private int unitatsTotals;
    private int unitatsPagades;

    public ValDescompte(int val, String name)
    {
        super(name);
        this.val = val;
    }

    public ValDescompte(int codi, String name, String tipus, String caducitat, int cmin, int var) {
        super(name);
        this.codi = codi;
        this.caducitat = caducitat;
        this.usat = false;
        tipusVal = tipus;
        this.compraMinima = cmin;
        if (tipus == "import total") this.importDescomptat = var;
        else this.percDesc = var;
    }

    public ValDescompte(int codi, String name, String tipus, String caducitat, String prod, int unitTot, int unitPag) {
        super(name);
        this.codi = codi;
        this.caducitat = caducitat;
        this.usat = false;
        this.tipusVal = tipus;
        this.producte = prod;
        this.unitatsTotals = unitTot;
        this.unitatsPagades = unitPag;
    }

    public int getVal() {
        return this.val;
    }

    public int getCodi() {
        return this.codi;
    }

    public String getCaducitat() {
        return caducitat;
    }

    public int getPercDesc() {
        return percDesc;
    }

    public int getUnitatsTotals() {
        return unitatsTotals;
    }

    public int getUnitatsPagades() {
        return unitatsPagades;
    }

    public boolean isUsat() {
        return usat;
    }

    public int getImportDescomptat() {
        return importDescomptat;
    }

    public int getCompraMinima() {
        return compraMinima;
    }

    public String getProducte() {
        return producte;
    }

    public void setUsat(boolean usat) {
        this.usat = usat;
    }

    @Override
    public String getTipusDescompte(){ return "Descompte de val"; }

    public String getTipusVal() {
        return tipusVal;
    }

}
