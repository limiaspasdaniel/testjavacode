package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


public class DescomptesService
{
    private DescomptesRepository descomptesRepository;
    private ArrayList<String> listPctsDesc = new ArrayList<>();
    public DescomptesService(DescomptesRepository descomptesRepository)
    {
        this.descomptesRepository = descomptesRepository;
    }

    public List<Descompte> listDescomptes()
    {
        return descomptesRepository.list();
    }

    public boolean existeixDescPct(String pctName) {
        for (String pct : listPctsDesc)
        {
            if (pct.equals(pctName)) return true;
        }
        return false;
    }

    public void insertDescPct(String pctName, String nameDescomp)
    {
        if(existeixDescPct(pctName))
        {
            throw new IllegalArgumentException("Aquest producte ja té un descompte al sistema");
        }

        descomptesRepository.findByName(nameDescomp).insertProduct(pctName);
        listPctsDesc.add(pctName);
    }

    public Descompte findByName(String name)
    {
        return descomptesRepository.findByName(name);
    }


    public XYDescompte createXYDescompte(int x, int y, String name)
    {
        if (x <= 0 || y <= 0){
            throw new IllegalArgumentException("X i Y han de ser mes grans que zero");
        }
        else if (y >= x){
            throw new IllegalArgumentException("Y no pot ser superior o igual a X");
        }
        else
        {
            XYDescompte xyd = new XYDescompte(x, y, name);
            descomptesRepository.insert(xyd);
            return xyd;
        }

    }

    public PercDescompte createPercDescompte(int perc, String name)
    {
        if(perc <= 0 || perc >= 100)
        {
            throw new IllegalArgumentException("Percentatge ha de ser mes gran que zero i més petit que 100");
        }
        else
        {
            PercDescompte percd = new PercDescompte(perc, name);
            descomptesRepository.insert(percd);
            return percd;
        }
    }

    public ValDescompte createValDescompte(int val, String name)
    {
        if(val <= 0)
        {
            throw new IllegalArgumentException("Val ha de ser mes gran que zero");
        }
        else
        {
            ValDescompte vald = new ValDescompte(val, name);
            descomptesRepository.insert(vald);
            return vald;
        }
    }

    //INICI VALS DESCOMPTE
    public boolean existeixVal(int codi) {
        return descomptesRepository.existeixVal(codi);
    }

    public ArrayList<ValDescompte> listValsDescompte()
    {
        return descomptesRepository.listValsDescompte();
    }

    public ValDescompte findByCode(int code)
    {
        return descomptesRepository.findByCode(code);
    }

    public void crearValDescompteTot(int codi, String name, String tipus, String caducitat, int cMin, int var) {
        if (codi <= 0) throw new IllegalArgumentException("El codi ha de ser més gran que zero");
        if (name.isEmpty()) throw new IllegalArgumentException("El nom no pot ser buit");
        if (caducitat.length() != 10 && !caducitat.isEmpty()) throw new IllegalArgumentException("La data de caducitat no és vàlida");
        if (cMin < 0) throw new IllegalArgumentException("El preu mínim de venda no és vàlid");
        if (tipus.equals("import total")) {
            if (var <= 0) throw new IllegalArgumentException("L'import descomptat ha de ser més gran que zero");
        }
        else if (tipus.equals("import total")) {
            if (var <=0 || var > 100) throw new IllegalArgumentException("El percentatge descomptat no és valid");
        }
        ValDescompte vd = new ValDescompte(codi, name, tipus, caducitat, cMin, var);
        descomptesRepository.insertVal(vd);
    }

    public void crearValDescompteXxY(int codi, String name, String tipus, String caducitat, String prod, int int1, int int2) {
        if (codi <= 0) throw new IllegalArgumentException("El codi ha de ser més gran que zero");
        if (name.isEmpty()) throw new IllegalArgumentException("El nom no pot ser buit");
        if (caducitat.length() != 10 && !caducitat.isEmpty()) throw new IllegalArgumentException("La data de caducitat no és vàlida");
        if (int2 < 0) throw new IllegalArgumentException("El nombre d'unitats pagades ha de ser més gran que zero");
        if (int1 <= int2) throw new IllegalArgumentException("El nombre d'unitats totals ha de ser més gran que el nombre d'unitats pagades");
        ValDescompte vd = new ValDescompte(codi, name, tipus, caducitat, prod, int1, int2);
        descomptesRepository.insertVal(vd);
    }

    public void clearListValsDescompte() {
        descomptesRepository.clearListValsDescompte();
    }

    //FI VALS DESCOMPTE

    /*public String getPctsInfo(Descompte discount, LinkedHashMap<String, Integer> infoDescPct, ProductsService productsService)
    {
        String name = discount.getName();

        StringBuffer m = new StringBuffer();
        m.append("Nom del descompte: " + name);
        m.append(System.getProperty("line.separator"));
        List listPcts = descomptesRepository.list();
        for(int i = 0; i < listPcts.size(); ++i)
        {
            Descompte item = (Descompte) listPcts.get(i);
            if(item.getName() == name)
            {
                m.append("Llista de productes amb aquest descompte:");
                m.append(System.getProperty("line.separator"));
                ArrayList<Integer> productes = item.getInfoProductes();
                for(int j = 0; j < productes.size(); ++j)
                {
                    int codi = productes.get(j);
                    if (j > 0) m.append(System.getProperty("line.separator"));
                    int unitat = infoDescPct.get(codi);
                    Product p = productsService.findByBarCode(codi);
                    int priceOneUnit = p.getPrice();
                    double preuOri = priceOneUnit*unitat;
                    int preuFinal = getPriceFinal(priceOneUnit, unitat, item);

                    m.append(codi + " amb el preu " + (int) preuOri + "€ aplicat el descompte " + "'" + name + "'" + ": " + preuFinal + "€");
                }
            }
        }
        return m.toString();
    }*/

    /*private int getPriceFinal(int priceU, int unitat, Descompte descompte)
    {
        double po = priceU*unitat;
        String tipusD = descompte.getTipusDescompte();
        int result = 0;
        if(tipusD.equals("Descompte en percentatge"))
        {
            int perc = descompte.getPerc();
            result = (int) (po * (double) (100-perc)/(double) 100);
        }
        else if(tipusD.equals("X per Y"))
        {
            int x = descompte.getX();
            int y = descompte.getY();
            if(x == unitat)
            {
                result = priceU*y;
            }
        }
        else if(tipusD.equals("Descompte de val"))
        {
            int val = descompte.getVal();
            result = (int) po - val;

        }
        return result;
    }*/



}
