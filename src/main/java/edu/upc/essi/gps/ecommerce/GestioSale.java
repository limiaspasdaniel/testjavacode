package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lei on 09/12/2015.
 */
public class GestioSale {
    private static List<Sale> sales;
    public GestioSale () {
        this.sales = new ArrayList<>();
    }
    public static void afegirSale(Sale sale) {
        sales.add(sale);
    }
    public static List<Sale> getSales() {return sales;}
}
