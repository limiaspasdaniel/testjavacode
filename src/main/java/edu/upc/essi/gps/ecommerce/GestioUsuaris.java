package edu.upc.essi.gps.ecommerce;

import java.util.Hashtable;

/**
 * Created by Daniel on 17/12/15.
 */
public class GestioUsuaris {

    private Hashtable<String, Usuari> usuaris;

    public Hashtable<String, Usuari> getUsuaris() {
        return usuaris;
    }

    public GestioUsuaris() {
        usuaris = new Hashtable<>();
    }

    public void afegirUsuari(String nom, Usuari u){
        usuaris.put(nom,u);
    }

    public Usuari getUsuari(String nom){
        return usuaris.get(nom);
    }

    boolean containsKey(String id){
        return usuaris.containsKey(id);
    }

    boolean containsUser(Usuari u){
        return usuaris.containsKey(u.getId());
    }

    public String getInfoVenedors(){
        String s = "";
        for (String key : usuaris.keySet()){
            if (usuaris.get(key).isVenedor()){
                s += usuaris.get(key).getId() + ";";
            }
        }
        return s;
    }
}
