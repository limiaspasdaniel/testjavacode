package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;

/**
 * Created by Francesc on 17/12/2015.
 */
public class CardPaymentService {
    private class Targeta {
        private int codi;
        private int pin;
        private boolean teFons;

        public Targeta (int codi, int pin, boolean fons) {
            this.codi = codi;
            this.pin = pin;
            this.teFons = fons;
        }

        public int getCodi() {
            return codi;
        }

        public void setCodi(int codi) {
            this.codi = codi;
        }

        public int getPin() {
            return pin;
        }

        public void setPin(int pin) {
            this.pin = pin;
        }

        public boolean teFons() {
            return teFons;
        }

        public void setTeFons(boolean fons) {
            this.teFons = fons;
        }
    }

    private ArrayList<Targeta> targetes;

    public CardPaymentService() {
        targetes = new ArrayList<>();
        Targeta t = new Targeta(123456789, -1, true);
        targetes.add(t);
        t = new Targeta(123456788, -1, false);
        targetes.add(t);
        t = new Targeta(987654321, 2468, true);
        targetes.add(t);
    }

    public void CardPayment(int cardCode, int amount, int pin) {
        for (Targeta t : targetes) {
            if (t.getCodi() == cardCode) {
                if (t.getPin() != pin) throw new IllegalArgumentException("Pin incorrecte");
                else if (!t.teFons()) throw new IllegalStateException("Sense Fons");
            }
        }
    }

    public boolean tePin(int cardCode) {
        for (Targeta t : targetes) {
            if (t.getCodi() == cardCode) {
                return (t.getPin() != -1);
            }
        }
        return false;
    }
}
