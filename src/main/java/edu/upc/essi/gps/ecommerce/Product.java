package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;
import edu.upc.essi.gps.domain.HasName;

public class Product implements Entity, HasName {

    private long id;
    private String name;
    private final int price;
    private final int vatPct;
    private final int barCode;
    private String nomDescomp;
    private String typeDescomp;
    private float popularity;
    private boolean isOnSale;

    public Product(long id, String name, int price, int vatPct, int barCode) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.vatPct = vatPct;
        this.barCode = barCode;
        isOnSale = true;
    }

    public void setProductInSale(boolean isOnSale){
        this.isOnSale = isOnSale;
    }

    public boolean isProductOnSale(){
        return isOnSale;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getPopularity() {
        return popularity;
    }

    //El price no hauria de ser un int, hauria de ser un double
    public int getPrice() {
        return price;
    }

    public int getVatPct() {
        return vatPct;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setDescomptePct(String type, String nomDescomp)
    {
        this.typeDescomp = type;
        this.nomDescomp = nomDescomp;
    }

    public String getNomDescomp()
    {
        return this.nomDescomp;
    }

    public float getPriceWithIva(){
        float val = price + (price * vatPct / 100);
        return val;
    }
}
