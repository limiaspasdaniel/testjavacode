package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by mingjian on 17/12/2015.
 */
public class PtsFedelitzacioService
{
    private LinkedHashMap<Soci, Integer> infoPtsSocis;

    public PtsFedelitzacioService()
    {
        this.infoPtsSocis = new LinkedHashMap<>();
    }

    public void addSoci(Soci s)
    {
        if(! infoPtsSocis.containsKey(s))
            infoPtsSocis.put(s, 0);
    }

    public int getPtsSoci(Soci s)
    {
        return infoPtsSocis.get(s);
    }

    public void setPtsSoci(Soci s, int pts)
    {
        infoPtsSocis.put(s, pts);
    }

    public int getMoneyFromPts(int pts)
    {
        int money = 0;
        if(pts > 0)
        {
            money = (int) ((double) pts/10);
        }
        return money;
    }

    public void resetPtsSoci(Soci s)
    {
        infoPtsSocis.put(s, 0);
    }


}
