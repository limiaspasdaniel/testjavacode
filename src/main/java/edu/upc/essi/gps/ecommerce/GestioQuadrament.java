package edu.upc.essi.gps.ecommerce;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Daniel on 26/11/15.
 */
public class GestioQuadrament {

    private List<Quadrament> quadraments;

    public GestioQuadrament() {
        quadraments = new ArrayList<>();
    }

    public GestioQuadrament(List<Quadrament> quadraments) {
        this.quadraments = quadraments;
    }

    public void afegirQuadrament(Quadrament q){
        quadraments.add(q);
    }

    public String visualitzarQuadraments(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yy");
        Collections.sort(quadraments);
        String info = "";
        //11-June-07 dni: 48094809Z, tpv: 1, diferencia: 100€;11-June-08 dni: 48094804Z, tpv: 2, diferencia: 150€;
        for (Quadrament q : quadraments){
            float diferencia = q.getDiferencia();

            if (diferencia > 0 || diferencia < 0){
                diferencia *= -1;
            }

            info += dateFormat.format(q.getData()) + " nom: " + q.getDniVenedor() + ", tpv: " + q.getTpv() + ", diferencia: " + diferencia+ "€;";
        }
        return info;
    }

    public Quadrament getQuadrament(String dniVenedor, int tpv, Date data){
        for (Quadrament q: quadraments){
            if (q.getData().equals(data) && q.getDniVenedor().equals(dniVenedor) && q.getTpv() == tpv) return q;
        }
        return null;
    }

    public void creaQuadrament(String dniVenedor, int tpv, Date data, float recompteInicial){
        Quadrament q = new Quadrament(recompteInicial, dniVenedor, tpv, data);
        quadraments.add(q);
    }

    public void plasmarIngres(String dniVenedor, int tpv, Date data, float ingres){
        for (Quadrament q: quadraments){
            if (q.getData().equals(data) && q.getDniVenedor().equals(dniVenedor) && q.getTpv() == tpv) {
                q.plasmarIngres(ingres);
            }
        }
    }

    public void introduirEfectiuFinalDia(String dniVenedor, int tpv, Date data, float imReal){
        for (Quadrament q: quadraments){
            if (q.getData().equals(data) && q.getDniVenedor().equals(dniVenedor) && q.getTpv() == tpv) q.introduirEfectiuFinal(imReal);
        }
    }

    public void tancarQuadramentIncorrecte(String dniVenedor, int tpv, Date data){
        for (Quadrament q: quadraments){
            if (q.getData().equals(data) && q.getDniVenedor().equals(dniVenedor) && q.getTpv() == tpv) q.tancarQuadramentIncorrecte();
        }
    }

    public String getMsgQuadrament(String dniVenedor, int tpv, Date data){
        for (Quadrament q: quadraments){
            if (q.getData().equals(data) && q.getDniVenedor().equals(dniVenedor) && q.getTpv() == tpv) return q.getMsgQuadrament();
        }
        return null;
    }

    public Quadrament getMostRecentQuadrament(){
        return quadraments.get(quadraments.size() - 1);
    }
}

