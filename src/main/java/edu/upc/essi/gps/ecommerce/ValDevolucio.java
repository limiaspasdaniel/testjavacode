package edu.upc.essi.gps.ecommerce;

import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Created by Lei on 30/11/2015.
 * suficiente con identificar con dataDevolucio, horaDevolucio y posNumber (tpv)
 * quiere decir que en la practica es "imposible" crear dos vals en el mismo dia, misma hora y mismo tpv
 */
public class ValDevolucio {
    private int quantitat;
    private String dataDevolucio;
    private String horaDevolucio;
    private String motiu;
    private Soci soci;
    private List <Product> productesDevolucio;
    private int posNumber;
    private String saleAssistantName;
    private boolean usat;

    public ValDevolucio (String dataDevolucio, String horaDevolucio, String motiu, Soci soci, List <Product> productesDevolucio, int posNumber, String saleAssistantName) {
        this.dataDevolucio = dataDevolucio;
        this.horaDevolucio = horaDevolucio;
        this.motiu = motiu;
        this.soci = soci;
        this.productesDevolucio = productesDevolucio;
        this.posNumber = posNumber;
        this.saleAssistantName = saleAssistantName;
        this.quantitat = 0;
        for (Product p : productesDevolucio) {
            this.quantitat += p.getPrice();
        }
        this.usat = false;
    }

    public int getQuantitat () {return this.quantitat;}
    public String getDataDevolucio () {return this.dataDevolucio;}
    public String getHoraDevolucio () {return this.horaDevolucio;}
    public String getMotiu () {return this.motiu;}
    public Soci getSoci () {return this.soci;}
    public List <Product> getProductesDevolucio () {return this.productesDevolucio;}
    public int getPosNumber () {return  this.posNumber;}
    public String getSaleAssistantName () {return this.saleAssistantName;}
    public void validarValDevolucio () {
        this.usat = true;
    }
    public boolean valUsat () {
        return this.usat;
    }
}
