package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lei on 22/11/2015.
 */
public class Soci {
    private String nom;
    private String cognom;
    private int edat;
    private int tel;
    private int punts;
    private List<Sale> compres;
    private List<Descompte> descomptes;
    /*
    Crea un soci.
    Post: crea un soci amb puntuacio 0. no te compres
     */
    public Soci (String nom, String cognom, int edat, int tel) {
        this.nom = nom;
        this.cognom = cognom;
        this.edat = edat;
        this.tel = tel;
        this.punts = 0;
        this.compres = new ArrayList<>();
        this.descomptes = new ArrayList<>();
    }

    public String getNom () {return this.nom;}
    public String getCognom () {return this.cognom;}
    public int getEdat () {return this.edat;}
    public int getTel () {return this.tel;}
    public int getPunts () {return this.punts;}

    public void afegirCompra (Sale compra) {
        this.compres.add(compra);
    }
    public List <Sale> getCompres () {return this.compres;}

    public void addDescompte(Descompte descompte)
    {
        this.descomptes.add(descompte);
    }

    public List<Descompte> getDescomptes() { return descomptes; }
}
