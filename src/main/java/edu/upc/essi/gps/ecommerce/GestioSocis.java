package edu.upc.essi.gps.ecommerce;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lei on 22/11/2015.
 */

public class GestioSocis {
    private List <Soci> socis;

    public GestioSocis () {
        this.socis = new ArrayList<>();
    }
    public List <Soci> getTots () {
        return this.socis;
    }

    public void afegir (String nom, String cognom, int edat, int telefon) {
        Soci soci = new Soci (nom, cognom, edat, telefon);
        this.socis.add(soci);
    }

    public int getNSocis () {
        return this.socis.size();
    }

    public Soci getSoci (String nom, String cognom, int telefon) {
        int i = 0;
        boolean trobat = false;
        Soci sociTrobat = null;
        while (i < this.socis.size() && !trobat) {
            Soci s = this.socis.get(i);
            if (nom.equals(s.getNom()) && cognom.equals(s.getCognom()) && telefon == s.getTel()) {
                trobat = true;
                sociTrobat = s;
            }
            i++;
        }
        if (sociTrobat == null) throw new IllegalStateException("No existeix el soci");
        else return sociTrobat;
    }
}
