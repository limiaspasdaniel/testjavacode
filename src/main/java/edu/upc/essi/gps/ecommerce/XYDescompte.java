package edu.upc.essi.gps.ecommerce;

public class XYDescompte extends Descompte
{
    private int x;
    private int y;

    public XYDescompte(int x, int y, String name)
    {
        super(name);
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return this.x;
    }

    public int getY()
    {
        return this.y;
    }

    @Override
    public String getTipusDescompte(){ return "Descompte X per Y"; }
}
