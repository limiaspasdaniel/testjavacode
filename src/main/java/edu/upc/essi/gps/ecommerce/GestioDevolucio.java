package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lei on 11/12/2015.
 */
public class GestioDevolucio {
    List<Devolucio> devolucions;
    public GestioDevolucio () {
        this.devolucions = new ArrayList<>();
    }

    public List<Devolucio> obteDevolucions () {
        return this.devolucions;
    }

    public boolean existeixDevolucio (String data, String hora, int posNumber) {
        for (Devolucio devolucio : this.devolucions) {
            if (devolucio.obteData().equals(data) && devolucio.obteHora().equals(hora) && devolucio.obtePosNumber() == posNumber) return true;
        }
        return false;
    }
    public void afegirDevolucio (Soci soci, String motiuDevolucio, List<Product> productesRetornades, String dataCreacio, String horaCreacio, int posNumber, String botiga, String venedor, ValDevolucio valDevolucio) {
        if (existeixDevolucio(dataCreacio, horaCreacio, posNumber)) throw new IllegalStateException("Ja existeix devolucio amb la data, hora i tpv donada");
        this.devolucions.add(new Devolucio(soci, motiuDevolucio, productesRetornades, dataCreacio, horaCreacio, posNumber, botiga, venedor, valDevolucio));
    }
    public Devolucio obteDevolucio (String data, String hora, int posNumber) {
        for (Devolucio devolucio : this.devolucions) {
            if (devolucio.obteData().equals(data) && devolucio.obteHora().equals(hora) && devolucio.obtePosNumber() == posNumber) return devolucio;
        }
        throw new IllegalStateException("Devolucio no existeix");
    }
}
