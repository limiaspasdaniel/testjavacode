package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;
import edu.upc.essi.gps.domain.HasName;

import java.util.ArrayList;

/**
 * Created by mingjian on 01/12/2015.
 */
public abstract class Descompte implements Entity, HasName
{
    private String name;
    private ArrayList<String> products;


    public Descompte(String name)
    {
        this.name = name;
        this.products = new ArrayList<>();
    }

    public ArrayList<String> getInfoProductes()
    {
        return this.products;
    }

    @Override
    public long getId()
    {
        return 0;
    }

    public ArrayList<String> getProducts()
    {
        return this.products;
    }

    public String getName() {
        return name;
    }

    public void insertProduct(String pctName)
    {
        this.products.add(pctName);
    }

    public int getX(){ return 0;}

    public int getY(){ return 0;}

    public int getPerc() { return 0; }

    public int getVal() { return 0; }

    public abstract String getTipusDescompte ();


}
