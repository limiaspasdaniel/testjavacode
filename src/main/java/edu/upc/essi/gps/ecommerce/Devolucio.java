package edu.upc.essi.gps.ecommerce;

import java.util.List;

/**
 * Created by Lei on 11/12/2015.
 */
public class Devolucio {
    private Soci soci;
    private float quantitatDiners;
    private String motiuDevolucio;
    private List<Product> productesRetornades;
    private String dataCreacio;
    private String horaCreacio;
    private int posNumber;
    private String botiga;
    private String venedor;
    private ValDevolucio valDevolucio;

    public Devolucio (Soci soci, String motiuDevolucio, List<Product> productesRetornades, String dataCreacio, String horaCreacio, int posNumber, String botiga, String venedor, ValDevolucio valDevolucio) {
        this.soci = soci;
        this.motiuDevolucio = motiuDevolucio;
        this.productesRetornades = productesRetornades;
        this.dataCreacio = dataCreacio;
        this.horaCreacio = horaCreacio;
        this.posNumber = posNumber;
        this.botiga = botiga;
        this.venedor = venedor;
        this.quantitatDiners = 0;
        this.valDevolucio = valDevolucio;
        for (Product product : productesRetornades) {
            quantitatDiners += product.getPriceWithIva();
        }
    }

    //soci nom, soci cognom, quantitat, motiu, datacreacio, horacreacio, posnumber, botiga, venedor, val devolucio
    //productes retornats
    public String obteInfo () {
        String e = " | ";
        StringBuilder info = new StringBuilder();
        if(soci != null) info.append(soci.getNom()).append(" ").append(soci.getCognom());
        else info.append("NO ES SOCI");
        info.append(e).append(quantitatDiners)
                .append(e).append(motiuDevolucio).append(e).append(dataCreacio).append(" ").append(horaCreacio).append(e)
                .append(posNumber).append(e).append(botiga).append(e).append(venedor).append(e);
        if (valDevolucio == null) info.append("NO");
        else info.append("SI");
        info.append(System.getProperty("line.separator")).append("PRODUCTES RETORNADES:").append(System.getProperty("line.separator"));
        for (Product product : productesRetornades) {
            info.append(product.getName()).append(System.getProperty("line.separator"));
        }
        return info.toString();
    }

    public String obteData () {
        return this.dataCreacio;
    }

    public String obteHora () {
        return this.horaCreacio;
    }

    public int obtePosNumber () {
        return this.posNumber;
    }

    public float obteQuantitatDiners () {
        return this.quantitatDiners;
    }
}
