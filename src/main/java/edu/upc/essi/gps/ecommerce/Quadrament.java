package edu.upc.essi.gps.ecommerce;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by xavi on 20/11/15.
 */
public class Quadrament implements Comparable<Quadrament> {

    private Date data;
    private float diferencia;
    private String dniVenedor;
    private int tpv;
    private float imEsperat;
    private float imReal;
    private float recompteInicial;
    boolean quadramentTancat;
    private String msgQuadrament;

    public String getDniVenedor() {
        return dniVenedor;
    }

    public void setDniVenedor(String dniVenedor) {
        this.dniVenedor = dniVenedor;
    }

    public int getTpv() {
        return tpv;
    }

    public void setTpv(int tpv) {
        this.tpv = tpv;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public float getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(float diferencia) {
        this.diferencia = diferencia;
    }

    /**Es crida al començar el dia, al fer el recompte d'efectiu del dia anterior, la data s'asigna automaticament**/
    public Quadrament(float imIntroduit, String dniVenedor, int tpv){
        data = new Date();
        imEsperat = imIntroduit;
        recompteInicial = imIntroduit;
        this.dniVenedor = dniVenedor;
        this.tpv = tpv;
        quadramentTancat = false;
    }

    /**Quadrament amb data predefinida**/
    public Quadrament(float imIntroduit, String dniVenedor, int tpv, Date data){
        imEsperat = imIntroduit;
        recompteInicial = imIntroduit;
        this.dniVenedor = dniVenedor;
        this.tpv = tpv;
        this.data = data;
        quadramentTancat = false;
    }

    public Quadrament() {
        quadramentTancat = false;
    }

    /**Es crida cada cop que el venedor fa una venda**/
    public void plasmarIngres(float imIntroduit){
        imEsperat += imIntroduit;
    }

    /**Es crida al final del dia per comprovar si la quantitat d'efectiu és la correcta**/
    public String calcularDiferencia(float imInroduit){
        diferencia = imEsperat - imInroduit;
        if (diferencia == 0.) msgQuadrament = ("Quadrament realitzat amb èxit");
        msgQuadrament = ("Quadrament incorrecte");
        return msgQuadrament;
    }

    public void introduirEfectiuFinal(float imReal){
        System.out.println("Import introduit pel venedor al final del dia: "+imReal);
        this.imReal = imReal;
        System.out.println("Import inicial: "+this.imEsperat);
        diferencia = imEsperat - imReal;
        if (diferencia == 0.) {
            quadramentTancat = true;
            msgQuadrament = ("Quadrament realitzat amb èxit");
        }
        else if ((diferencia > 0 && diferencia < 1) || (diferencia < 0 && diferencia > -1)){
            msgQuadrament = ("Quadrament incorrecte per menys de 1€");
        }
        else msgQuadrament = ("Quadrament incorrecte per més de 1€");
    }

    public void tancarQuadramentIncorrecte(){
        quadramentTancat = true;
        msgQuadrament = ("Quadrament incorrecte realitzat");
    }

    public String getMsgQuadrament(){
        return msgQuadrament;
    }


    //No entenc la funció d'això
    @Override
    public int compareTo(Quadrament o) {
        return getData().compareTo(o.getData());
    }

    public boolean mateixQuadrament (int valor) {
        int esperat = (int)this.imEsperat;
        return (valor == esperat);
    }
}
