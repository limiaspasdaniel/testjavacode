package edu.upc.essi.gps.ecommerce;

/**
 * Created by xavi on 17/12/15.
 */
public class Reserva {
    private long prodId;
    private int dniComprador;
    private String prodName;
    private int quantitat;

    public Reserva(long prodId, int dniComprador, String prodName, int quantitat) {
        this.prodId = prodId;
        this.dniComprador = dniComprador;
        this.prodName = prodName;
        this.quantitat = quantitat;
    }

    public long getProdId() {
        return prodId;
    }

    public int getDniComprador() {
        return dniComprador;
    }

    public String getProdName() {
        return prodName;
    }

    public int getQuantitat() {
        return quantitat;
    }
}
