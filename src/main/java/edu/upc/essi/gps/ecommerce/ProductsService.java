package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Comparators;

import java.util.List;

public class ProductsService {

    private ProductsRepository productsRepository;

    public ProductsService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    public void newProduct(String name, int price, int vatPct, int barCode){
        if (barCode < 0) throw new IllegalStateException("ERROR: Codi de barres menor que 0.");
        if(price < 0) throw new IllegalArgumentException("ERROR: Preu menor que 0.");
        if(vatPct <  0) throw new IllegalArgumentException("ERROR: IVA menor que 0.");
        if(this.findByBarCode(barCode) != null)
        {
            throw new IllegalArgumentException("Ja existeix un producte amb codi de barres "+barCode+".");
        }
        long id = productsRepository.newId();
        Product result = new Product(id,name, price, vatPct, barCode);
        productsRepository.insert(result);
    }

    public void newProduct(String name, int price, int vatPct, int barCode, boolean isOnSale){
        if (barCode < 0) throw new IllegalStateException("ERROR: Codi de barres menor que 0.");
        if(price < 0) throw new IllegalArgumentException("ERROR: Preu menor que 0.");
        if(vatPct <  0) throw new IllegalArgumentException("ERROR: IVA menor que 0.");
        if(this.findByBarCode(barCode) != null)
        {
            throw new IllegalArgumentException("Ja existeix un producte amb codi de barres "+barCode+".");
        }
        long id = productsRepository.newId();
        Product result = new Product(id,name, price, vatPct, barCode);
        result.setProductInSale(false);
        productsRepository.insert(result);
    }

    public List<Product> listProducts(){
        return productsRepository.list();
    }

    public List<Product> listProductsByName(){
        return productsRepository.list(Comparators.byName);
    }

    public Product findByName(String productName) {
        return productsRepository.findByName(productName);
    }

    public Product findByBarCode(int barCode) {
        return productsRepository.findByBarCode(barCode);
    }
}
