package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;

import java.util.*;

class SaleLine{
    private long productId;
    private String productName;
    private int unitPrice;
    private int amount;
    private int iva;
    private int barCode;
    private String discountName;
    private int totalPrice;
    private int descXYAplicat;

    public SaleLine(Product product, int amount) {
        this.productId = product.getId();
        this.productName = product.getName();
        this.unitPrice = product.getPrice();
        this.amount = amount;
        this.iva = product.getVatPct();
        this.barCode = product.getBarCode();
        this.discountName = product.getNomDescomp();
        this.totalPrice = this.unitPrice*this.amount;
        this.descXYAplicat = 0;
    }

    public int getIva() { return iva; }

    public int getBarCode() { return barCode; }

    public long getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public int getAmount() {
        return amount;
    }

    public int getTotalPrice() {
        return this.unitPrice*this.amount;
    }

    public int getPreuTotal() { return totalPrice; }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getDescXYAplicat() {
        return descXYAplicat;
    }

    public void setDescXYAplicat(int descXYAplicat) {
        this.descXYAplicat = descXYAplicat;
    }

    public void incUnits(int unitats) {
        ++amount;
    }

    public String getDiscountName() { return discountName; }


}

public class Sale implements Entity
{
    private final String shop;
    private String preu;
    private String date;
    private String time;
    private int change;
    private String chargeType;
    private final int posNumber;
    private final String saleAssistantName;
    private List<SaleLine> lines = new LinkedList<>();
    private List<SaleLine> aux_lines = new LinkedList<>();
    private LinkedHashMap <Integer, Integer> ivas;
    private LinkedHashMap <String, LinkedHashMap <String, Integer>> infoDescPcts;
    private LinkedHashMap <String, ArrayList<Product>> descuentos;
    private int entregat;
    private ValDescompte valDescompteVenda;
    private int descomptatVals;
    private int descompteDevolucio;
    private DescomptesService descomptesService;

    public void addProduct(Product p, int unitat) {
        SaleLine line = new SaleLine(p,unitat);
        aux_lines.add(line);
        boolean present = false;
        for(SaleLine l : lines){
            if (l.getProductName() == p.getName() ) {
                present = true;
                l.incUnits(unitat);
                l.setTotalPrice(l.getAmount()*l.getUnitPrice());
            }
        }
        if (!present) {
            line.setTotalPrice(line.getAmount()*line.getUnitPrice());
            lines.add(line);
        }

        String nomDesc = p.getNomDescomp();
        String nomPct = p.getName();
        if (nomDesc != null) {
            if (! descuentos.containsKey(nomDesc)) {
                descuentos.put(nomDesc, new ArrayList<>());
            }
            else if(descuentos.containsKey(nomDesc))
            {
                for (int i = 0; i < unitat; ++i)
                {
                    descuentos.get(nomDesc).add(p);
                }
            }
        }

        String pctName = p.getName();
        if(nomDesc != null)
        {
            if(infoDescPcts.containsKey(nomDesc))
            {
                LinkedHashMap<String, Integer> infoPct = infoDescPcts.get(nomDesc);
                Integer numPct = unitat;
                if(infoPct.containsKey(pctName))
                {
                    numPct += infoPct.get(pctName);
                }
                infoPct.put(pctName, numPct);
            }
            else
            {
                LinkedHashMap<String, Integer> infoNewPct = new LinkedHashMap<>();
                infoNewPct.put(pctName, unitat);
                infoDescPcts.put(nomDesc, infoNewPct);
            }
            Descompte desc = descomptesService.findByName(nomDesc);
            if (!desc.getTipusDescompte().equals("Descompte de val")) calcularDescompte(line, desc);
        }

        int unit = line.getAmount();
        int price = line.getUnitPrice();
        int iva = line.getIva();
        int priceUnitat = price*unit;

        if (ivas.containsKey(iva))
            priceUnitat += ivas.get(iva);

        ivas.put(iva, priceUnitat);

        if (valDescompteVenda != null) recalcularDescompteVal(line);
    }

    public Sale(String shop, int posNumber, String saleAssistantName, String data, String hora, DescomptesService ds) {
        this.shop = shop;
        this.posNumber = posNumber;
        this.saleAssistantName = saleAssistantName;
        ivas = new LinkedHashMap<>();
        infoDescPcts = new LinkedHashMap<>();
        descuentos = new LinkedHashMap<>();
        lines = new LinkedList<>();
        aux_lines = new LinkedList<>();
        this.date = data;
        this.time = hora;
        descomptatVals = 0;
        descomptesService = ds;
    }

    public Sale(String shop, String preu, String saleAssistantName, String data, String hora, int posNumber) {
        this.shop = shop;
        this.preu = preu;
        this.saleAssistantName = saleAssistantName;
        this.date = data;
        this.time = hora;
        this.descuentos = new LinkedHashMap<> ();
        this.infoDescPcts = new LinkedHashMap<>();
        this.posNumber = posNumber;
    }

    public String getDate() { return date; }

    public String getTime() { return time; }

    public String getShop() {
        return shop;
    }

    public int getPosNumber() {
        return posNumber;
    }

    public String getSaleAssistantName() {
        return saleAssistantName;
    }

    public List<SaleLine> getLines() {
        return Collections.unmodifiableList(lines);
    }

    public List<SaleLine> getLinesForPrint() {
        return this.aux_lines;
    }

    public LinkedHashMap<String, LinkedHashMap<String, Integer>> getInfoDescPcts() { return infoDescPcts; }

    public LinkedHashMap <String, ArrayList<Product>> getDescuentos() {return descuentos;}


    public String getPreu() {return preu; }

    public int getTotalSenseDescompte() {
        int res = 0;
        for(SaleLine l : lines){
            res += l.getTotalPrice();
        }
        return res;
    }
    public int getTotal() {
        int res = 0;
        for(SaleLine l : lines){
            res += l.getTotalPrice();
        }
        return res-(descomptatVals+descompteDevolucio);
    }

    public LinkedHashMap<Integer, Integer> getIvas () {
        return ivas;
    }

    public int getEntregat()
    {
        return entregat;
    }

    public void setEntregat(int delivery)
    {
        this.entregat = delivery;
    }

    public int getChange()
    {
        return this.entregat - getTotal();
    }

    public void setChange(int canvi)
    {
        this.change = canvi;
    }

    public boolean isEmpty() {
        return lines.isEmpty();
    }

    public String getChargeType()
    {
        return this.chargeType;
    }

    public int getDescomptatVals() {
        return descomptatVals;
    }

    public void setChargeType(String type)
    {
        this.chargeType = type;
    }


    public ValDescompte getValDescompteVenda() {
        return valDescompteVenda;
    }

    public void addValDescompte(ValDescompte vd) {
        valDescompteVenda = vd;
        if (vd.getTipusVal().equals("import total")) descomptatVals = vd.getImportDescomptat();
        else if (vd.getTipusVal().equals("percentatge total")) descomptatVals = (getTotal()/vd.getPercDesc());
        else {
            for (SaleLine sl : lines) {
                if (sl.getProductName().equals(vd.getProducte())) {
                    int numProductesAfectats = sl.getAmount();
                    while (numProductesAfectats%(vd.getUnitatsTotals()) != 0){
                            --numProductesAfectats;
                    }
                    while (numProductesAfectats > 0) {
                        descomptatVals += sl.getUnitPrice();
                        numProductesAfectats -= vd.getUnitatsTotals();
                    }
                }
            }
        }
    }

    public void recalcularDescompteVal(SaleLine sl) {
        if (!valDescompteVenda.getTipusVal().equals("import total")) {
            descomptatVals = 0;
            addValDescompte(valDescompteVenda);
        }
    }

    public void calcularDescompte(SaleLine sl, Descompte desc) {
        if (desc.getTipusDescompte().equals("Descompte en percentatge")) {
            int basePrice = sl.getAmount() * sl.getUnitPrice();
            sl.setTotalPrice(basePrice-basePrice/desc.getPerc());
        }
        else calcularDescompteXxY(desc);
    }

    public void calcularDescompteXxY(Descompte desc) {
        ArrayList<String> pDesc = desc.getInfoProductes();
        int numProductesAfectats = 0;
        for (SaleLine sl : this.getLines()) {
            if (pDesc.contains(sl.getProductName())) numProductesAfectats += sl.getAmount();
        }
        if (numProductesAfectats >= desc.getX()) {
            for (SaleLine sl : this.getLines()) {
                if (pDesc.contains(sl.getProductName())) sl.setDescXYAplicat(0);
            }
            while (numProductesAfectats%(desc.getX()) != 0){
                --numProductesAfectats;
            }
            while (numProductesAfectats > 0) {
                int minPreu = 0;
                String prodCandidat = "";
                for (SaleLine sl : this.getLines()) {
                    if (pDesc.contains(sl.getProductName()) && (sl.getTotalPrice() - sl.getDescXYAplicat()) > 0) {
                        if (minPreu == 0) {
                            minPreu = sl.getUnitPrice();
                            prodCandidat = sl.getProductName();
                        }
                        else if (minPreu > sl.getUnitPrice()) {
                            minPreu = sl.getUnitPrice();
                            prodCandidat = sl.getProductName();
                        }
                    }
                }
                for (SaleLine sl : this.getLines()) {
                    if (prodCandidat.equals(sl.getProductName())) {
                        sl.setDescXYAplicat(sl.getDescXYAplicat()+minPreu);
                        sl.setTotalPrice(sl.getUnitPrice()*sl.getAmount()-sl.getDescXYAplicat());
                    }
                }
                numProductesAfectats -= desc.getX();
            }
        }
    }

    public void aplicarValDevolucio (int quantitat) {
        this.descompteDevolucio += quantitat;
    }

    public boolean hihaValDevolucio () {
        return (this.descompteDevolucio > 0);
    }

    public int obteDescompteDevolucio () {
        return this.descompteDevolucio;
    }

    public int getPreuTotal() {
        int res = 0;
        for(SaleLine l : lines){
            res += l.getPreuTotal();
        }
        return res-(descomptatVals+descompteDevolucio);
    }

    @Override
    public long getId() {
        return 0;
    }


}
