package edu.upc.essi.gps.ecommerce;

public class Usuari {
    public static enum Tipus {ADMIN, GESTOR, VENEDOR};
    private Tipus tipus;
    private String id;
    private String password;

    public Usuari(Tipus tipus, String id, String password){
        this.tipus = tipus;
        this.id = id;
        this.password = password;
    }

    public Tipus getTipus() { return tipus; }

    public boolean isAdmin() { return tipus == Tipus.ADMIN; }

    public boolean isGestor() { return tipus == Tipus.GESTOR; }

    public boolean isVenedor() { return tipus == Tipus.VENEDOR; }

    public String getId() { return id; }

    public String getPassword() { return password; }
}
