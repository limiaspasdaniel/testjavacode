## Iteració actual [19]

- Com a venedor vull poder fer una venda per tal de vendre els productes de la botiga. [3]

	- El venedor pot crear una nova venda amb una confirmació al sistema.

	- El sistema crea una nova venda, amb el nom de l’usuari de la sessió i la data de la creació.

	- El venedor pot afegir productes amb els seus codis a la venda iniciada.

	- El sistema afegeix a la llista de productes de la venda iniciada.

- Com a venedor vull afegir producte a una venda per tal d’enregistrar les compres d’un client. [3]

	- El sistema permet crear nou producte.

	- El venedor introdueix les dades de producte. 

	- El sistema enregistra el producte.

- Com a venedor vull poder cobrar una venda per tal de generar ingressos per la botiga.[8]

	- El venedor pot cobrar una venda en efectiu

	- L’usuari paga en efectiu

	- El venedor enregistra l’efectiu del client

	- El sistema li avisa del canvi d’efectiu a retornar al client si n’hi ha

- Com a venedor vull poder imprimir un tiquet de compra per tal de complir la llei mercantil.[5]

	- El sistema mostra la llista de productes que ha comprat el client amb el nom, nombre d’unitat, i el preu.

	- El sistema mostra els diferents descomptes aplicats a cada producte.

	- El sistema mostra la forma de pagament.

	- El sistema mostra la informació general com la informació de terminal, la informació sobre el venedor, i la data, hora de tiquet. 