###PRODUCT BACKLOG

1. Com a gestor de la botiga vull poder gestionar els venedors de la botiga per tal de controlar els meus empleats. [3]
Comprovar que:
	- El sistema permet crear nous venedors.
	- El sistema mostra els venedors existents en el sistema.
	- Al crear un nou venedor, s’introdueix el dni. Si aquest ja existeix en el sistema, el sistema dona error i deixa de crear-lo; en el cas contrari, el sistema el crea i li assigna un identificador d’usuari i una contrasenya.
	- Fent clic sobre un venedor es mostra el seu detall.
	- El sistema confirma al gestor de la botiga la creació de venedors.

2. Com a gestor de la botiga vull poder afegir nous productes al sistema per tal de controlar quins productes es venen a la botiga.[5]
Comprovar que:
	- Si el nou producte introduït ja existeix en el sistema, el sistema dona error i deixa de crear el nou producte; en el cas contrari, el sistema crea el nou producte.
	- El sistema confirma al gestor de la botiga de la creació del nou producte.
	
3. Com a gestor de la botiga vull poder fer descomptes en certs productes per tal de facilitar la venda d'aquests productes. [5]
Comprovar que:
	- El sistema mostra tots els productes existents en una llista.
El gestor de la botiga escull els productes que vol aplicar descompte.
	- El gestor de la botiga escull el tipus de descompte i introdueix la quantitat numèrica de cada tipus: en tants per cent, en una quantitat fixa, i/o “compra X quantitats, paga Y quantitats”, on X és major que Y.
	- El sistema comprova que no hi ha cap descompte d’el tipus seleccionat per al producte indicat.
	- El sistema confirma al gestor de la botiga de l’aplicació de descompte als productes seleccionats.
	
4. Com a gestor de la botiga vull veure els socis de la botiga per tal de fidelitzar els clients.
Comprovar que:
	- El sistema mostra els socis existents en el sistema en una llista.
	- Fent clic sobre un soci es mostra el seu detall. (DNI, nom, mail …).

5. Com a venedor vull poder identificar-me en el tpv que faci servir en un dia per tal d’iniciar el meu torn.
Comprovar que:
	- El sistema demana identificador d’usuari i la contrasenya del venedor per iniciar la sessió.
	- El sistema carrega les dades necessàries per crear un entorn de treball pel venedor.
	- El sistema notifica al venedor si les dades són correctes per iniciar la sessió.
	
6. LOGOUT VENEDOR

7. *Com a venedor vull poder fer una venda per tal de vendre els productes de la botiga.
Comprovar que:
	- El venedor pot crear una nova venda amb una confirmació al sistema.
	- El sistema crea una nova venda, amb el nom de l’usuari de la sessió i la data de la creació.
	- El venedor pot afegir productes amb els seus codis a la venda iniciada.
	- El sistema afegeix a la llista de productes de la venda iniciada.

8. AFEGIR PRODUCTE VENDA

9. Com a venedor vull poder eliminar un producte d'una venda per tal de eliminar un producte que el client decideixi que ja no vol.[3]
	- El venedor pot treure eliminar un producte d’una venda iniciada.
	- El sistema pot llistar productes enregistrats d’una venda.






